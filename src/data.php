<?php
	//print_r($_POST);
	require('_def.php');
	
	$id = $httpget->getString('iddb');
	
	$conn = $dbs->getDBConn($id);

	
	$tb = $httpget->getString('tb');
	$fields = $httpget->getString('fields');
	$fld = $httpget->getString('fld');
	$vl = $httpget->getString('vl');
	
	$act = $httpget->getString('act');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Debaser - <?php echo($conn->database); ?> - <?php echo($tb); ?></title>
		<link href="styles.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="/js/jstree/dist/themes/default/style.min.css" />
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
		
		<script type="text/javascript" src="js/data.js"></script>
		<script type="text/javascript" src="js/drag.js"></script>
		<script type="text/javascript" src="js/log.js"></script>
		<script type="text/javascript" src="js/debaser.js"></script>
		<script type="text/javascript" src="js/dbs-win.js"></script>
		<script type="text/javascript" src="js/mngTable.js"></script>
		<script type="text/javascript" src="js/mngField.js"></script>

		<script src="/js/jstree/dist/jstree.min.js"></script>
	</head>
	<body id="bodyNode">
		<div id="wintemplate" class="jdraggable"></div>
		<div id="loadAlertWin" class="alertWin">Loading...</div>
		<div id="logWin" class="jdraggable" style="top:50px; left:200px">
			<div class="tablename">Log</div>
			<div id="logBody" class="logWin"></div>
			<input type="button" value="clear" onclick="clearLog();" />
			<input type="button" value="close" onclick="hideLogWin();" />
		</div>
		<?php
			if ($act == 'ins') {
				$rsF1 = $conn->getFields($tb);
				
				$sqlc = "";
				$sqlv = "";
				
				while($row = mysqli_fetch_array($rsF1)) {
					if (isset($_POST[$row[0]]) && strlen($_POST[$row[0]])) {
						if (strlen($sqlc) > 0) {
							$sqlc .= ", ";
							$sqlv .= ", ";
						}
						
						$sqlc .= "`" . $row[0] . "`";
						$sqlv .= "'" . $conn->escape($_POST[$row[0]]) . "'";
					}
				}
				
				$sql = "INSERT INTO $tb ($sqlc) VALUES ($sqlv)";
				
				$conn->execute($sql);
				
				$lerr = mysqli_error();
				if (strlen($lerr) > 0) {
					echo($lerr);
				}
			}

			$sql = 'SELECT ';

			if ($fields) {
				$table_fields = explode(',', $fields);
				$sql .= $fields;}
			else {
				$table_fields = $conn->getFields($tb);
				$sql .= '*';
			}

			$sql .= ' FROM ' . $tb;

			$filtro = '';
			if ($fld && $vl) {
				$filtro = " WHERE $fld = '$vl'";
				$sql = $filtro;
			}

			$sql .= ' LIMIT 100';

			$rsData = $conn->getData($sql);
			
			$r = mysqli_num_fields($rsData);
			
			$cor1 = '66B8FB';
			$cor2 = '99D0FD';
			
			$cor = $cor1;
		?>
		<div>tabela: <strong><?php echo($tb); ?></strong>&nbsp;<a href="#" onclick="openFilter(<?php echo($id); ?>, '<?php echo($tb); ?>'); return false;">alterar/filtrar</a><br><br>
		filtro: <?php echo($filtro); ?><br><br>
		registos: <?php echo($conn->w); ?>&nbsp;<a href="#" onclick="openInsert(<?php echo($id); ?>, '<?php echo($tb); ?>'); return false;">novo</a><br>
		<br></div>
		<table border="0" cellpadding="4" cellspacing="0">
			<tr style="background-color:#0000FF;">
				<td class="branco">&nbsp;|&nbsp;</td>
				<?php foreach($table_fields as $fld) {
					echo("<th class=\"branco\">&nbsp;" . $fld . "&nbsp;</th>");
					echo("<td class=\"branco\">&nbsp;|&nbsp;</td>");
				} ?>
			</tr>
			<?php
				$numR = 0;
				while($row = mysqli_fetch_array($rsData, MYSQLI_ASSOC)) {
					
					//print_r($row);
					
					echo("<tr bgcolor='#$cor'>");
					echo("<td valign='top' align='center'>|</td>");
					
					for ($i = 0; false; $i++) {
					//for ($i = 0; $i < $r; $i++) {
						
						$tipo_campo = $conn->getFieldType(mysqli_field_name($rsData, $i));
						
						echo("<td valign='top' nowrap>");
						//echo($row[$i]);
						?><!--&nbsp;<a href="javascript:setJumpData('<?php echo(mysqli_field_name($rsData, $i)); ?>', '<?php echo($row[$i]); ?>')">-&gt;</a>--><?php
						?><div id="v<?php echo(mysqli_field_name($rsData, $i)); ?>_<?php echo($numR); ?>" style="display: inline;"><?php echo($row[$i]); ?></div>&nbsp;<a href="#" onclick="showFieldCtxMenu('v<?php echo(mysqli_field_name($rsData, $i)); ?>_<?php echo($numR); ?>', '<?php echo(mysqli_field_name($rsData, $i)); ?>', '<?php echo($row[$i]); ?>'); return false;">+</a>
						<?php
						echo("</td><td valign='top' align='center'>|</td>");
						
						++$numR;
					}
					
					foreach($row as $fld => $val) {
						
						$tipo_campo = $conn->getFieldType($tb, $fld);
						
						if ($conn->isBigTextType($tipo_campo)) {
							$val = "-- TEXTO --";
						}
						
						echo("<td valign='top' nowrap>");
						//echo $fld. " # " . $tipo_campo . " # " . $val;
						
						?><div id="v<?php echo $fld; ?>_<?php echo($numR); ?>" style="display: inline;"><?php echo $val; ?></div>&nbsp;<a href="#" onclick="showFieldCtxMenu('v<?php echo $fld; ?>_<?php echo($numR); ?>', '<?php echo $fld; ?>', '<?php echo $val; ?>'); return false;">+</a>
						<?php
						echo("</td><td valign='top' align='center'>|</td>");
					}
					
				echo("</tr>");
				if ($cor == $cor1) {
					$cor = $cor2;
				} else {
					$cor = $cor1;
				}
			}
			$r *= 2;
			$r++;
			
			$rsFields = $conn->getFields($tb);
			?>
			<!--tr bgcolor="#<?php echo($cor) ?>"><td colspan="<?php echo($r) ?>">&nbsp;</td></tr-->
			<tr bgcolor="#0000FF"><td colspan="<?php echo($r) ?>">&nbsp;</td></tr>
		</table>
        <br>
        <br>
		
		<textarea cols="50" name="txtSQL" rows="5"><?php echo($sql); ?></textarea>
		<br />
		<div id="fieldCtxMenu" class="jdraggable">
			<table border="0" cellpadding="2" cellspacing="0">
                <tr style="background-color:#0000FF;">
                  <th class="branco" style="white-space: nowrap;"><div id="fieldCtxMenuTitle">[title]&nbsp;</div></th>
                  <th class="branco"><img src="im/clear.gif" alt="" border="0" onclick="hideFieldCtxMenu(); return false;" title="Close"></th>
                </tr>
				<tr><td colspan="2"><a href="#" onclick="setJumpData(); return false;">Set Filter</a></td></tr>
				<tr><td colspan="2"><a href="#" onclick="renderLink(); return false;">Render as link</a></td></tr>
				<tr><td colspan="2"><a href="#" onclick="renderImage(); return false;">Render as image</a></td></tr>
				<tr><td colspan="2"><a href="#" onclick="renderOriginal(); return false;">Render original</a></td></tr>
				
				<!-- tr><td><a href="#">Render as image</a></td></tr -->
			</table>
			<div id="fieldCtxMenuFormBox">
				<form id="fieldCtxMenuForm" action="#" onsubmit="return false;">
					<input type="hidden" name="fId" value="" />
					<input type="hidden" name="fName" value="" />
					<input type="hidden" name="fValue" value="" />
				</form>
			</div>
		</div>
		
		<script type="text/javascript">
			function showHidePhpVars() {
				tdiv = document.getElementById('phpVars');
				disp = tdiv.style.display;
				
				if (disp == 'none') {tdiv.style.display = 'block';}
				else {tdiv.style.display = 'none';}
			}
	   </script>
	   <a href="#" onclick="showHidePhpVars(); return false;">php vars</a>
	   <div id="phpVars" style="display: none;">

        <div id="jstree_demo_div">
        <?php
            $all_vars = get_defined_vars();
            Var_Dump_Tree_View::var_dump_object($all_vars);
        ?>
        </div><hr>

	   <script>$(function () { $('#jstree_demo_div').jstree(); });</script>
   </div>
	</body>
</html>
