<?php
	date_default_timezone_set("Europe/Lisbon");

	header("Access-Control-Allow-Origin: *");

	session_start();
	
	function getToDocRoot() {
		$scriptName = $_SERVER['SCRIPT_NAME'];
		
		$counts = substr_count($scriptName, "/");
		
		if ($counts < 2) {
			return ".";
		}
		
		$str = str_repeat("/..", ($counts - 1));
		$str = substr($str, 1);
		
		return $str;
	}
	
	if (!defined('TO_DOC_ROOT')) {
		define('TO_DOC_ROOT', getToDocRoot()); // esta constante é global :)
	}
	
	function my_autoloader($class_name) {
		$dirs = array("class");

		$count = 0;
		$file_target = "";
		$file_target_ok = "";
		foreach($dirs as $d) {
			$file_target = TO_DOC_ROOT . "/" . $d . "/" . $class_name . ".class.php";

			if (is_file($file_target)) {
				++$count;
				$file_target_ok = $file_target;
			}
		}

		if ($count > 1) {
			exit("ERROR: Several class definition with the same name [$class_name].");
		} elseif ($count == 1) {
			require_once($file_target_ok);
		}
	}

	spl_autoload_register('my_autoloader');

	require("_init.php");
