var logWin;
var logBody;

function setLogWins() {
	if (!logWin) {
		logWin = document.getElementById('logWin');
	}
	if (!logBody) {
		logBody = document.getElementById('logBody');
	}
}

function showLogWin() {
	setLogWins();
	logWin.style.visibility = 'visible';
}

function hideLogWin() {
	setLogWins();
	logWin.style.visibility = 'hidden';
}

function clearLog() {
	setLogWins();
	
	logBody.innerHTML = '';
}

function log(txt) {
	setLogWins();
	logBody.innerHTML += txt + '<br />';
}
