
function openNewTableForm(id) {
	openWin('newTableForm', 'templates/newTableForm.php?iddb=' + id);
}

function doCreateNewTable(f) {
	callExec('cmd/createTable.php?iddb=' + f.hIDDB.value + '&tableName=' + f.tableName.value + '&pkField=' + f.pkField.value);
}

function dropTableForm(f) {
	if (confirm('Drop table \'' + f.lstTable.value + '\' ?')) {
		callExec('cmd/dropTable.php?iddb=' + f.hIDDB.value + '&tableName=' + f.lstTable.value);
	}
}