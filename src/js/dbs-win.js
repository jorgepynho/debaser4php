
function showDBList() {
	openWin('dblist', 'templates/db_conns.php');
}

function showDBCompareWin() {
	openWin('dbcomp', 'templates/db_compare.php');
}

function showNewConnList() {
	openWin('newConnList', 'templates/new_db_conn.php');
}

function showSQLWin() {
	openWin('SQLWin', 'templates/sql.php');
}

function showAppList() {
	openWin('applist', 'templates/apps.php');
}

function addTableList(iddb) {
	openWin('tblist' + iddb, 'templates/tableList.php?iddb=' + iddb);
}

var newWinName;

function openTable(f) {
	newWinName = 'table_db' + f.hIDDB.value + 'tb' + f.lstTable.value;
	
	openWin(newWinName, 'templates/tb.php?iddb=' + f.hIDDB.value + '&ptb=' + f.lstTable.value);
}

function openAllTables(f) {
	
	if (!confirm('Tem a certeza que deseja abrir todas as tabelas ?')) {
		return false;
	}
	
	$('#formTables option').each(function(index, element) {
		//alert(element.text);
		
		newWinName = 'table_db' + f.hIDDB.value + 'tb' + element.text;
		
		openWin(newWinName, 'templates/tb.php?iddb=' + f.hIDDB.value + '&ptb=' + element.text);
	});
}

function openTableX(f) {
	newWinName = 'tablex_db' + f.hIDDB.value + 'tb' + f.lstTable.value;
	
	openWin(newWinName, 'templates/tbx.php?iddb=' + f.hIDDB.value + '&ptb=' + f.lstTable.value);
}

function openModel(f) {
	newWinName = 'model_db' + f.hIDDB.value + 'tb' + f.lstTable.value;
	
	openWin(newWinName, 'templates/model.php?iddb=' + f.hIDDB.value + '&ptb=' + f.lstTable.value);
}

function openFlat(f) {
	newWinName = 'flat_db' + f.hIDDB.value + 'tb' + f.lstTable.value;
	
	openWin(newWinName, 'templates/flat.php?iddb=' + f.hIDDB.value + '&ptb=' + f.lstTable.value);
}

function openData(id, tab) {
	newWinName = 'data_db' + id + 'tb' + tab;
	
	//alert(newWinName);
	
	openWin(newWinName, 'templates/data.php?iddb=' + id + '&ptb=' + tab);
	
	nw = document.getElementById(newWinName);
	
	if (nw) {
		//alert(nw.style.width);
		//nw.style.width = '90%';
		//nw.style.overflow = 'auto';
	} else {
		//alert('no window ????');
	}
}

function openInsert(id, tab) {
	newWinName = 'insert_db' + id + 'tb' + tab;
	url = 'templates/insert.php?iddb=' + id + '&ptb=' + tab;
	
	//alert(newWinName);
	//alert(url);
	
	openWin(newWinName, url);
}

function openFilter(id, tab) {
	newWinName = 'filter_db' + id + 'tb' + tab;
	
	//alert(newWinName);
	
	openWin(newWinName, 'templates/filter.php?iddb=' + id + '&ptb=' + tab);
}

function openMenus(ida) {
	newWinName = 'menuIDA_' + ida;
	
	//alert(ida);
	
	openWin(newWinName, 'templates/menus.php?idapp=' + ida);
}