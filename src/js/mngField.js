function openNewFieldForm(f) {
	openWin('newFieldForm', 'templates/newFieldForm.php?iddb=' + f.hIDDB.value + '&tb=aaa');
}

function doCreateNewField(f) {
	callExec('cmd/createField.php');
	//callExec('cmd/createField.php?iddb=' + f.hIDDB.value + '&tableName=' + f.tableName.value + '&pkField=' + f.pkField.value);
}

function dropField(f) {
	if (confirm('Drop field \'' + f.lstTable.value + '\' ?')) {
		callExec('cmd/dropField.php?iddb=' + f.hIDDB.value + '&tableName=' + f.lstTable.value);
	}
}