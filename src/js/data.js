
var fcm;

//function setJumpData(c, v) {
function setJumpData() {

	fj = document.getElementById('formJump');
	cf = document.getElementById('fieldCtxMenuForm');
	
	fj.fld.value = cf.fName.value;
	fj.val.value = cf.fValue.value;
	
	hideFieldCtxMenu();
	
	return false;
}

function formJumpOK(f) {
	return true;
}

function formDataOK(f) {
	return true;
}

// show field context menu
function showFieldCtxMenu(divElem, nameX, valueX) {
	div = document.getElementById(divElem);
	fcm = document.getElementById('fieldCtxMenu');
	
	if (!fcm) {
		return false;
	}
	
	fcmt = document.getElementById('fieldCtxMenuTitle');
	fcmt.innerHTML = 'field: ' + nameX;
	
	f = document.getElementById('fieldCtxMenuForm');
	f.fId.value = div.id;
	f.fName.value = nameX;
	f.fValue.value = valueX;
	
	//fcm.style.top = getMouseY();
	//fcm.style.left = getMouseX();
	
	fcm.style.top = findPosY(div);
	fcm.style.left = findPosX(div) + 50;
	
	fcm.style.visibility = 'visible';
}

function findPosX(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return curleft;
}

function findPosY(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return curtop;
}

function hideFieldCtxMenu() {
	fcm = document.getElementById('fieldCtxMenu');
	
	if (!fcm) {
		return false;
	}
	
	fcm.style.visibility = 'hidden';
}


var IE = document.all?true:false

// If NS -- that is, !IE -- then set up for mouse capture
if (!IE) document.captureEvents(Event.MOUSECLICK)

function getMouseX(e) {
  if (IE) { // grab the x-y pos.s if browser is IE
    tempX = event.clientX + document.body.scrollLeft
    
  } else {  // grab the x-y pos.s if browser is NS
    tempX = e.pageX;
    
  }  
  
  // catch possible negative values in NS4
  if (tempX < 0){tempX = 0}
  

  return tempX;
}

function getMouseY(e) {
  if (IE) { // grab the x-y pos.s if browser is IE

    tempY = event.clientY + document.body.scrollTop
  } else {  // grab the x-y pos.s if browser is NS

    tempY = e.pageY;
  }  
  // catch possible negative values in NS4
  
  if (tempY < 0){tempY = 0}  

  return tempY;
}

function renderLink() {
	cf = document.getElementById('fieldCtxMenuForm');
	
	fxId = cf.fId.value;
	fxName = cf.fName.value;
	fxValue = cf.fValue.value;
	
	targetDiv = document.getElementById(fxId);
	
	if (!targetDiv) {
		alert(fxId + ' n�o existe.');
		return false;
	}
	
	targetDiv.innerHTML = '<a href="' + fxValue + '">' + fxValue + '</a>';
	
	hideFieldCtxMenu();
}

function renderImage() {
	cf = document.getElementById('fieldCtxMenuForm');
	
	fxId = cf.fId.value;
	fxName = cf.fName.value;
	fxValue = cf.fValue.value;
	
	targetDiv = document.getElementById(fxId);
	
	if (!targetDiv) {
		alert(fxId + ' n�o existe.');
		return false;
	}
	
	//alert('path = ' + fxValue);
	
	targetDiv.innerHTML = '<img src="' + fxValue + '" border="2" />';
	
	hideFieldCtxMenu();
}

function renderOriginal() {
	cf = document.getElementById('fieldCtxMenuForm');
	
	fxId = cf.fId.value;
	fxName = cf.fName.value;
	fxValue = cf.fValue.value;
	
	targetDiv = document.getElementById(fxId);
	
	if (!targetDiv) {
		alert(fxId + ' n�o existe.');
		return false;
	}
	
	//alert('path = ' + fxValue);
	
	targetDiv.innerHTML = fxValue;
	
	hideFieldCtxMenu();
}

function clearDataForm(f) {
	f.fld.value = '';
	f.val.value = '';
	f.orderBy.value = '';
}