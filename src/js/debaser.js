var BODY;
var winTemplate;
var winCount = 0;
var loadAlertWin;

function setBody() {
	if (!BODY) {BODY = document.getElementById('bodyNode');}
}

function setLoadAlertWin() {
	if (!loadAlertWin) {
		loadAlertWin = document.getElementById('loadAlertWin');
	}
}

function showLoadAlertWin() {
	setLoadAlertWin();
	loadAlertWin.style.visibility = 'visible';
}

function hideLoadAlertWin() {
	setLoadAlertWin();
	loadAlertWin.style.visibility = 'hidden';
}

function execXhr(winId, url, callback) {
	var objReq;
	
	showLoadAlertWin();
	
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		objReq = new XMLHttpRequest();
	} else if (window.ActiveXObject) { // IE
		objReq = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	objReq.onreadystatechange = function() {
		if (objReq.readyState == 4) {
			if (objReq.status == 200) {
				//alert(objReq.responseText);
				callback(winId, objReq.responseText);
			} else {
				alert("There was a problem retrieving the data:\n" + objReq.statusText);
			}
			hideLoadAlertWin();
		}
	};
	
	log('GET ' + url);
	
	objReq.open("GET", url, true);
	objReq.send(null);
}

function setWinXY(obj) {
	
	var startX = 50;
	var startY = 100;
	
	var newX = 0;
	var newY = 0;
	
	var modW = 0;
	
	modW = winCount % 10;
	
	newX = startX + (winCount * 50);
	newY = startY + (modW * 100);
	
	$(obj).animate({
		top: newX + 'px',
		left: newY + 'px'
	}, 'fast');
}

function createWin(winName) {
	newWin = document.getElementById(winName);
	
	if (!newWin) {
		
		if (!winTemplate) {
			winTemplate = document.getElementById('wintemplate');
		}
		
		newWin = winTemplate.cloneNode(true);
		newWin.id = winName;
		setWinXY(newWin);
		
		setBody();
		BODY.appendChild(newWin);
		
		++winCount;
		
		$('.jdraggable').draggable({ handle: "#headbar" });
	}
	
	return newWin;
}

function openWin(winName, url) {
	//alert(winName);
	//alert(url);
	
	createWin(winName);
		
	useURL = url;
	
	if (useURL.indexOf('?') < 0) {
		useURL = useURL + '?';
	} else {
		useURL = useURL + '&';
	}
	
	useURL = useURL + 'winId=' + winName;
	
	execXhr(winName, useURL, handleWin);
}

function handleWin(winName, content) {
	newWin = createWin(winName);
	
	newWin.innerHTML = content;
	newWin.style.visibility = 'visible';
}

function closeWin(winId) {
	var twin = document.getElementById(winId);
	
	if (!twin) return;
	
	twin.style.visibility = 'hidden';
	
	twin.innerHTML = '[hidden empty window]';
}

function callExec(url) {
	execXhr('', url, handleExec);
}

function handleExec(winName, content) {
	alert(content);
}

function view_data(a) {
	var form = document.getElementById('formTables');

	var tb = form.lstTable.value;
	var iddb = form.hIDDB.value;

	a.href = 'data.php?iddb=' + iddb + '&tb=' + tb;

	return true;
}

function setACT(f, act) {
	f.hACT.value = act;
}

function actions(f) {
	var act = f.hACT.value;
	
	if (act == 'DATA') {
		f.action = 'data.php';
		return true;
	}
	
	if (act == 'GEN_PHP') {
		f.action = 'gen/php.php';
		return true;
	}
	
	if (act == 'GEN_ASP') {
		f.action = 'gen/asp.php';
		return true;
	}
	
	if (act == 'GEN_JAVA') {
		f.action = 'gen/java.php';
		return true;
	}
	
	if (act == 'GEN_SQL') {
		f.action = 'gen/sql.php';
		return true;
	}
	
	return false;
}

function newConnFormSubmit(f) {
	
	$.ajax({
		url: 'cmd/new_conn.php',
		type: 'POST',
		data: 'server=' + f.server.value + '&usern=' + f.usern.value + '&passw=' + f.passw.value + '&db=' + f.db.value,
		processData: false,
		//async: false,
		global: false,
		error: function() {
			alert('error (new conn Ajax POST)');
		},
		success: function(data ) {
			//alert('success');
			
			if (data == 'OK') {
				
				f.server.value = ''
				f.usern.value = ''
				f.passw.value = ''
				f.db.value = ''
				
				alert('New connection saved')
				
				showDBList();
			} else {
				alert('ERR: ' + data);
			}
		},
		complete: function() {
			//alert('complete');
		}
	});
	
	return false;
}

function delConn(id) {
	
	if (!confirm('Tem a certeza ?')) return false;
	
	$.ajax({
		url: 'cmd/del_conn.php',
		data: 'id=' + id,
		type: 'POST',
		processData: false,
		global: false,
		error: function() {
			alert('error (del conn Ajax POST)');
		},
		success: function(data ) {
			if (data == 'OK') {
				alert('Connection deleted')
				showDBList();
			} else {
				alert('ERR: ' + data);
			}
		},
		complete: function() {
			//alert('complete');
		}
	});
	
	return false;
}
