<?php
	require('_def.php');

	//error_reporting(E_ALL);
	//ini_set('display_errors', true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>Debaser Home - DB compare</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<?php
		//exit;
		
		function compareTableFields($rs1, $rs2) {
			
			$row_count1 = mysqli_num_rows($rs1);
			$row_count2 = mysqli_num_rows($rs2);
			
			echo $row_count1 . ' - ' . $row_count2 . '<br>' . PHP_EOL;
			
			if ($row_count1 != $row_count2) {
				return false;
			}
			
			$loops = 0;
			
			do {
				$row1 = mysqli_fetch_assoc($rs1);
				$row2 = mysqli_fetch_assoc($rs2);
				
				unset($row1['TABLE_CATALOG']);
				unset($row2['TABLE_CATALOG']);
				
				unset($row1['TABLE_SCHEMA']);
				unset($row2['TABLE_SCHEMA']);
				
				if (!$row1 || !$row2) return false;
				
				if (count($row1) != count($row2)) {
					return false;
				}
				
				//print_r($row1);
				//print_r($row2);
				
				foreach($row1 as $k => $r1) {
					if ($r1 != $row2[$k]) return false;
				}
				
				foreach($row2 as $k => $r2) {
					if ($r2 != $row1[$k]) return false;
				}
				
				++$loops;
				
			} while($row1 || $row2 || $loops < 9999);
			
			echo $loops;
			
			return true;
		}
		
		function compareTableSingleField() {
		}
		
		$server1 = $httppost->getString('server1');
		$usern1 = $httppost->getString('usern1');
		$passw1 = $httppost->getString('passw1');
		$db1 = $httppost->getString('db1');
		
		$server2 = $httppost->getString('server2');
		$usern2 = $httppost->getString('usern2');
		$passw2 = $httppost->getString('passw2');
		$db2 = $httppost->getString('db2');

		$conn1 = new MPConn($server1, $usern1, $passw1, $db1);
		$conn2 = new MPConn($server2, $usern2, $passw2, $db2);

		$table_names1 = $conn1->getTablesNames();
		$table_names2 = $conn2->getTablesNames();
		
		$tables = array_merge($table_names1, $table_names2);
		$tables = array_unique($tables);
		
		?><table border="1" cellpadding="3" cellspacing="0">
			<tr>
				<th>&nbsp;</th>
				<th>1</th>
				<th>2</th>
				<th>Fields</th>
			</tr>
		<?php
		
		foreach($tables as $tb) {
			
			$isTb_conn1 = $conn1->isTable($tb);
			$isTb_conn2 = $conn2->isTable($tb);
			
			$fc = false;
			$flds_result_txt = '-';
			
			?><tr>
				<td><?php echo $tb; ?></td>
				<td><?php
					if ($isTb_conn1) {
						?><img src="/im/green_check.png" width="16" height="16" alt="">
						<?php
					} else {
						?><img src="/im/red_check.png" width="16" height="16" alt="">
						<?php
					}
				?></td>
				<td><?php
					if ($isTb_conn2) {
						?><img src="/im/green_check.png" width="16" height="16" alt="">
						<?php
					} else {
						?><img src="/im/red_check.png" width="16" height="16" alt="">
						<?php
					}
				?></td>
				<td>
					<?php
						/*$rs1 = $conn1->getFieldsInfo($tb, 'COLUMN_NAME');
						$rs2 = $conn2->getFieldsInfo($tb, 'COLUMN_NAME');
						
						$fc = compareTableFields($rs1, $rs2);
						
						if ($fc) {
							$flds_result_txt = 'OK';
						} else {
							$flds_result_txt = 'NOT';
						}
						
						echo $flds_result_txt;*/
						
						if ($isTb_conn1 && $isTb_conn2) {
					?>
					
					<table border="1" cellpadding="3" cellspacing="0">
						<tr>
							<td>&nbsp;</td>
							<td>1</td>
							<td>2</td>
							<td title="field attributes with diferences">info</td>
						</tr>
					
					<?php
						$fields1 = $conn1->getFieldsInfoKeysArray($tb);
						$fields2 = $conn2->getFieldsInfoKeysArray($tb);
						
						if (!$fields1) $fields1 = array();
						if (!$fields2) $fields2 = array();
						
						$flds_names_1 = array_keys($fields1);
						$flds_names_2 = array_keys($fields2);
						
						/*echo('<pre>');
						echo print_r($fields1, true);
						echo('</pre>');*/
						//print_r($fields2);
						
						$tb_fields_both = array_merge($flds_names_1, $flds_names_2);
						
						$tb_fields_both = array_unique($tb_fields_both);
						
						foreach($tb_fields_both as $k => $f) {
							//$fld_info1 = $conn1->getFieldInfo($tb, $f);
							//$fld_info2 = $conn2->getFieldInfo($tb, $f);
							
							//echo $k . '<br>' . PHP_EOL;
							
							//print_r($fld_info1);
							//print_r($fld_info2);
							
							$has_field_1 = array_key_exists($f, $fields1);
							$has_field_2 = array_key_exists($f, $fields2);
							
							echo "<tr>" . PHP_EOL;
							echo "<td>" . $f . "</td>" . PHP_EOL;
							echo "<td>";
							
							if ($has_field_1) {
								?><img src="/im/green_check.png" width="16" height="16" alt="">
								<?php
							} else {
								?><img src="/im/red_check.png" width="16" height="16" alt="">
								<?php
							}
							
							echo "</td>" . PHP_EOL;
							echo "<td>";
							
							if ($has_field_2) {
								?><img src="/im/green_check.png" width="16" height="16" alt="">
								<?php
							} else {
								?><img src="/im/red_check.png" width="16" height="16" alt="">
								<?php
							}
							
							echo "</td>" . PHP_EOL;
							
							echo "<td>";
							
							if ($has_field_1 && $has_field_2) {
								
								$field_info_difs = 0;
								
								foreach($fields1[$f] as $k2 => $v1) {
									
									if ($k2 == 'TABLE_CATALOG') continue;
									if ($k2 == 'TABLE_SCHEMA') continue;
									
									$v2 = $fields2[$f][$k2];
									
									//echo $v1 . "<br>";
									//echo $v2 . "<br><br>";
									
									if ($v1 != $v2) {
										echo $k2 . "<br>" . PHP_EOL;
										++$field_info_difs;
									}
								}
							} else {
								echo "-";
							}
							
							echo "</td>" . PHP_EOL;
							
							echo "</tr>" . PHP_EOL;
						}
					?>
					
					</table>
					
					<?php } ?>
				</td>
			</tr>
			<?php
		}
		
		?></table><?php
	?>
	<div>
		<pre><?php
			//print_r($tables);
		?></pre>
    </div>
	
		<script type="text/javascript">
			function showHidePhpVars() {
				tdiv = document.getElementById('phpVars');
				disp = tdiv.style.display;
				
				if (disp == 'none') {tdiv.style.display = 'block';}
				else {tdiv.style.display = 'none';}
			}
	   </script>
	   <a href="#" onclick="showHidePhpVars(); return false;">php vars</a>
	   <div id="phpVars" style="display: none;">
	   <pre><?php print_r(get_defined_vars()); ?></pre>
</body>
</html>
<?php
	$conn1->close();
	$conn2->close();
