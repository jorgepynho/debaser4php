<?php
	require('../_def.php');
	
	$winId = $httpget->getString('winId');
	
	$databases = $dbs->getDBs();
?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr id="headbar">
	  <td colspan="2"><table width="100%" border="0" cellpadding="2" cellspacing="0">
        <tr>
          <td class="tablename">Connections</td>
		  <td class="tablename" align="right">
		  	<a href="#" onclick="showDBList(); return false;" title="refresh"><img src="im/ref.gif" alt="" /></a>
			<a href="#" onclick="closeWin('<?php echo($winId); ?>'); return false;" title="close"><img src="im/clear.gif" border="0" alt="" /></a>
		  </td>
        </tr>
      </table></td>
	</tr>
	<tr><td colspan="2"><table cellpadding="2" cellspacing="0" border="0">
		<?php foreach($databases as $k => $conn) { ?>
			<tr>
				<th colspan="4"><?=$conn['host'] ?> : <?=$conn['username'] ?></th>
			</tr>

			<?php foreach ($conn['dbs'] as $k2 => $db) { ?>
				<tr>
					<td><?=$db ?></td>
					<td><a href="#" onclick="addTableList('<?=$k.':'.$k2 ?>'); return false;" title="table list"><img src="im/tb.jpg" alt="tables"></a></td>
				    <td><a href="#" onclick="showSQLWin('<?=$k.':'.$k2 ?>'); return false;" title="SQL window"><img src="im/sql.jpg" alt="sql"></a></td>
				</tr>
			<?php } ?>
			
		<?php } ?>
	</table>
	    </td>
	</tr>
</table>
