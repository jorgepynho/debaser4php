<?php
	require('../_def.php');
	
	$id = $httpget->getString('iddb');
	$tab = $httpget->getString('ptb', 0);
	$winId = $httpget->getString('winId');
	
	$conn = $dbs->getDBConn($id);
?>
<table border="0" cellpadding="2" cellspacing="0">
	<tr id="headbar">
		<td class="tablename"><?php echo($tab); ?>&nbsp;-&nbsp;new</td>
		<td class="tablename" align="right"><img src="im/clear.gif" border="0" onclick="closeWin('<?php echo($winId); ?>')"></td>
	</tr>
	<tr>
		<td valign="top" colspan="2">
<form action="data.php?act=ins" method="post" name="formTable" id="formTable" onsubmit="return formDataOK(this);">
              <table class="insertData-table" border="0" cellpadding="4" cellspacing="0">
                <?php
				$flds = $conn->getFields($tab);
				while($row = mysqli_fetch_array($flds)) {
					
					//print_r($row);
					
				echo("<tr>");
				echo("<td valign=\"top\">" . $row[0] . "&nbsp;</td>");
				
				echo("<td>");
				$type = $dbs->getInputType($id, $tab, $row[0]);
				
				if ($type == false) {
					echo("<input type=\"text\" name=\"" . $row[0] . "\" value=\"" . $row[4] . "\">");
				} else {
					if ($type['type'] == "select") {
						echo("<select name=\"" . $row[0] . "\">");
						
						if ($type['aux1'] == "foreignTable") {
							$sqlx = "SELECT " . $type['aux3'] . ", " . $type['aux4'] . " FROM " . $type['aux2'];
							
							echo($sqlx);
							
							$fx = $conn->getData($sqlx);
							while($f = mysqli_fetch_array($fx)) {
								?><option value="<?php echo($f[$type['aux3']]); ?>"><?php echo(htmlentities($f[$type['aux3']] . " - " . $f[$type['aux4']], ENT_QUOTES)); ?></option>
								<?php
							}
						}
						
						if ($type['aux1'] == "array") {
							$vals = split(",", $type['aux2']);
							$txts = split(",", $type['aux3']);
							
							$vs = count($vals);
							$ts = count($txts);
							
							$max = $vs;
							if ($ts < $max) {
								$max = $ts;
							}
							
							for ($ix = 0; $ix < $max; ++$ix) {
								?><option value="<?php echo($vals[$ix]); ?>"><?php echo($txts[$ix]); ?></option>
								<?php
							}
						}
						
						if ($type['aux1'] == "enum") {
							$sqlx = "SHOW FIELDS FROM $tb LIKE '" . $type['field'] . "'";
							$fx = $conn->getObject($sqlx);
							if ($fx == false) {
								?><option value="0">!!! ERRORS in inputs !!!</option>
								<?php
							} else {
								$enums = $fx->Type;
								$enums = str_replace("enum(", "", $enums);
								$enums = str_replace(")", "", $enums);
								
								$arr = split(",", $enums);
								$aux = 2;
								foreach($arr as $e) {
									$e = str_replace("'", "", $e);
									?><option value="<?php echo($e); ?>"><?php echo($e . " - " . $type["aux$aux"]); ?></option>
									<?php
									++$aux;
								}
							}
						}
						
						echo("</select>");
					}
					
					if ($type['type'] == "textarea") {
						?><textarea name="<?php echo($row[0]); ?>"></textarea>
						<?php
					}
				}
				
				$fld_info = array();
				
				if ($row['Default']) $fld_info[] = '<span title="default">' . $row['Default'] . '</span>';
				if ($row['Key'] == 'PRI') $fld_info[] = '<span title="primary key">PK</span>';
				if (strtolower($row['Extra']) == 'auto_increment') $fld_info[] = '<span title="auto increment">AI</span>';
				
				if ($fld_info) echo ' ' . implode(',', $fld_info);
				
				echo("</td>");
				
				echo("</tr>\n");
			} ?>
                <tr>
                  <td>&nbsp;<input type="hidden" id="lstTable" name="lstTable" value="<?php echo($tab); ?>">
				  <input type="hidden" id="hIDDB" name="hIDDB" value="<?php echo($id); ?>">
				  </td>
                  <td><input type="submit" value="Gravar"></td>
                </tr>
              </table>
			  	<!--label>
			  <input type="checkbox" name="keepOpenWin" value="1" checked>
			  manter janela aberta
			  </label-->
            </form>
		</td>
	</tr>
</table>