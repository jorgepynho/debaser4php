<?php
	require('../_def.php');
	
	$iddb = $httpget->getString('iddb');
	$winId = $httpget->getString('winId');
	
	$conn = $dbs->getDBConn($iddb);
	
	$tab = $httpget->getString('ptb'); //nome da tabela
?>
<form id="formTbx" name="formTbx" method="post" action="#" style="display:inline;">
<input name="hIDDB" type="hidden" id="hIDDB" value="<?php echo($iddb); ?>">
<input name="lstTable" type="hidden">
<table border="0" cellpadding="2" cellspacing="0">
	<tr id="headbar">
		<td class="tablename"><?php echo($tab); ?>&nbsp;<a href="#" onclick="openNewFieldForm(document.forms['formTbx']); return false;" title="new field">+</a></td>
		<td class="tablename" align="right">&nbsp;<img src="im/clear.gif" border="0" onclick="closeWin('<?php echo($winId); ?>')" alt="Close"></td>
	</tr>
	<tr>
		<td vAlign="top" colspan="2">
<table border="0" cellpadding="2" cellspacing="0">
	<?php
		$fields = $conn->getFieldsInfo($tab);
		if ($fields) {
			while($fld = mysqli_fetch_array($fields)) {
				?>
				<tr><td><?php
					if ($fld['COLUMN_KEY'] == 'PRI') {
						?><img src="im/pk.jpg"><?php
					}
				?>&nbsp;</td>
				<td class="verdana14" valign="top"><?php echo($fld['COLUMN_NAME']); ?>&nbsp;</td>
				<td class="verdana14" valign="top">&nbsp;<?php echo($fld['COLUMN_TYPE']); ?></td>
				</tr>
	<?php
			}
		}
	?>
<!--tr>
	  <td>&nbsp;</td>
	  <td colspan="2" class="verdana10" valign="top"><input name="button" type="button" onclick="openNewFieldForm(this.form)" value="new" /></td>
	  </tr-->
</table>
		</td>
	</tr>
</table>
</form>