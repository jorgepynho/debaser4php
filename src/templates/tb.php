<?php
	require('../_def.php');
	
	$iddb = $httpget->getString('iddb');
	$winId = $httpget->getString('winId');
	
	$conn = $dbs->getDBConn($iddb);
	
	$tab = $httpget->getString('ptb'); //nome da tabela
?>
<form id="formTb" name="formTb" method="post" action="#" style="display:inline;">
<input name="hIDDB" type="hidden" id="hIDDB" value="<?php echo($iddb); ?>">
<input name="lstTable" type="hidden">
<table border="0" cellpadding="2" cellspacing="0">
  <tr id="headbar">
    <td class="tablename"><?php echo($tab); ?>&nbsp;<a href="#" onclick="openNewFieldForm(document.forms['formTb']); return false;" title="new field">+</a></td>
    <td class="tablename" align="right">&nbsp;<img src="im/clear.gif" alt="Close" border="0" onclick="closeWin('<?php echo($winId); ?>')"></td>
  </tr>
  <tr>
    <td valign="top" colspan="2"><table border="0" cellpadding="2" cellspacing="0">
      <?php
		$fields = $conn->getFieldsInfo($tab);
		if ($fields) {
			while($fld = mysqli_fetch_array($fields)) {
				?>
      <tr>
        <td><?php if ($fld['COLUMN_KEY'] == 'PRI') { ?>
            <img src="im/pk.jpg" alt="Primary Key">
        <?php } ?>&nbsp;</td>
        <td colspan="2" class="verdana14" valign="top"><?php echo($fld['COLUMN_NAME']); ?></td>
      </tr>
      <?php
			}
		}
	?>
    </table></td>
  </tr>
</table>
</form>
