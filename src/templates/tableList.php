<?php
	require('../_def.php');

	$iddb = $httpget->getString('iddb');

	$winId = $httpget->getString('winId');

	$conn = $dbs->getDBConn($iddb);
?><table border="0" cellpadding="2" cellspacing="0">
	<tr id="headbar">
		<td class="tablename"><?php echo($conn->host. "." . $conn->database); ?>&nbsp;&nbsp;</td>
		<td class="tablename" align="right">
		  	<a href="#" onclick="addTableList(<?php echo($iddb); ?>); return false;" title="refresh"><img src="im/ref.gif" alt="" /></a>
			<a href="#" onclick="closeWin('<?php echo($winId); ?>'); return false;"><img src="im/clear.gif" border="0" alt="" /></a>
		</td>
	</tr>
	<tr>
		<td vAlign="top" colspan="2">
		
		<a href="#" onclick="openNewTableForm(<?php echo $iddb; ?>); return false;">new table</a>&nbsp;|&nbsp;<a href="#">new view</a>&nbsp;|&nbsp;<a href="#">new routine</a><br><br>
		
<form action="#" method="post" name="formTables" target="_blank" id="formTables" onsubmit="return actions(this);">
<table border="0" cellpadding="2" cellspacing="0">
	<tr><td colspan="8">
	<select name="lstTable">
	<?php
		$tbs = $conn->getTables();
		if ($tbs) {
			while($tx = mysqli_fetch_array($tbs)) {
				?><option value="<?php echo($tx[0]); ?>"><?php echo($tx[0]); ?></option>
				<?php
			}
		}

		$tbs = $conn->getViews();
		if ($tbs) {
			while($tx = mysqli_fetch_array($tbs)) {
				?><option value="<?php echo($tx[0]); ?>"><?php echo($tx[0]); ?></option>
				<?php
			}
		}
	?>
	</select>
	<input name="cmdDropTable" type="button" id="cmdDropTable" onclick="dropTableForm(this.form);" value="drop">
	<input name="hIDDB" type="hidden" id="hIDDB" value="<?php echo($iddb); ?>">
	<input name="hACT" type="hidden" id="hACT" value=""></td>
	</tr>
	<tr>
	<td align="right"><input name="cmdT" type="button" id="cmdT" onclick="openTable(this.form);" value="T"></td>
	<td align="right"><input name="cmdT" type="button" id="cmdTa" onclick="openAllTables(this.form);" value="Ta"></td>
	<td align="right"><input name="cmdTP" type="button" id="cmdTP" onclick="openTableX(this.form);" value="Tx"></td>
	<td align="right"><input name="cmdM" type="button" id="cmdM" onclick="openModel(this.form);" value="M"></td>
	<td align="right"><input name="cmdF" type="button" id="cmdF" onclick="openFlat(this.form);" value="F"></td>
	<td align="right"><a href="data.php" onclick="return view_data(this);" target="_blank" title="view data"><img src="im/tb.jpg" alt=""></a></td>
	<td align="right"><input name="cmdGenSQL" type="image" id="cmdGenSQL" src="im/sql.jpg" onclick="setACT(this.form, 'GEN_SQL');" title="view SQL code"></td>
	<td align="right"><input name="cmdGenJava" type="image" id="cmdGenJava" src="im/java.gif" onclick="setACT(this.form, 'GEN_JAVA');" title="view Java code"></td>
	<td align="right"><input name="cmdGenPHP" type="image" id="cmdGenPHP" src="im/php.gif" onclick="setACT(this.form, 'GEN_PHP');" title="view PHP code"></td>
	</tr>
</table></form>
<br>

<a href="/dump/schema.php?iddb=<?php echo $iddb; ?>" target="_blank">dump schema</a>&nbsp;|&nbsp;
<a href="/dump/data.php?iddb=<?php echo $iddb; ?>" target="_blank">dump all data</a>&nbsp;|&nbsp;
<a href="/dump/reset.php?iddb=<?php echo $iddb; ?>" target="_blank">reset data script</a>

		</td>
	</tr>
</table>