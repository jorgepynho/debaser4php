<?php
	require('../_def.php');
	
	$id = $httpget->getString('iddb');
	$ptab = $httpget->getString('ptb');
	$winId = $httpget->getString('winId');

	$conn = $dbs->getDBConn($id);
?>
<form action="data.php?act=filter&amp;iddb=<?=$id ?>&amp;tb=<?=$ptab ?>" method="post" id="formJump" onsubmit="return formJumpOK(this)">
  <table border="0" cellpadding="4" cellspacing="0">
	<tr id="headbar">
		<td class="tablename"><?php echo($ptab); ?>&nbsp;-&nbsp;filtro</td>
		<td class="tablename" align="right"><img src="im/clear.gif" border="0" onclick="closeWin('<?php echo($winId); ?>')"></td>
	</tr>
	<tr>
	  <td>Tabela</td>
	  <td><?php
		$tables = $conn->getTables();
		echo("<select name=\"lstTable\">");
		while($tab = mysqli_fetch_array($tables)) {
			if ($ptab == $tab[0]) {
				echo("<option value=\"" . $tab[0] . "\" selected>" . $tab[0] . "</option>");
			} else {
				echo("<option value=\"" . $tab[0] . "\">" . $tab[0] . "</option>");
			}
		}
		echo("</select>");
	?></td>
	</tr>
	<tr>
	  <td>Campo</td>
	  <td><input name="fld" type="text" id="fld" value="" /></td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td><select name="opr">
		<option value="=" selected="selected">=</option>
		<option value="&lt;">&lt;</option>
		<option value="&gt;">&gt;</option>
		<option value="&lt;&gt;">&lt;&gt;</option>

		<option value="LIKE">LIKE</option>
		<option value="IN">IN</option>
	  </select></td>
	</tr>
	<tr>
	  <td>Valor</td>
	  <td><input name="val" type="text" id="val"></td>
	</tr>
	<tr>
	  <td>Order By</td>
	  <td><input name="orderBy" type="text" id="orderBy" /></td>
	</tr>
	<tr>
	  <td>&nbsp;<input type="hidden" id="hIDDB" name="hIDDB" value="<?php echo($id); ?>"></td>
	  <td><input name="cmdData" type="submit" id="cmdData" value="Data" />
	  <input name="cmdReset" type="reset" id="cmdReset" value="Repor" />
	  <input name="cmdLimpar" type="button" id="cmdLimpar" value="Limpar" onclick="clearDataForm(this.form);" /></td>
	</tr>
  </table>
</form>