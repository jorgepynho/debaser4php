<?php
	session_start();
	
	date_default_timezone_set("Europe/Lisbon");
	
	include("../inc/rq.php");
	
	$id = sRQ('iddb', 0);
	$tab = sRQ('ptb', 0);
	$winId = sRQ('winId', 0);
	
	include("../func/date.func.php");
	
	include("../class/Logs.class.php");
	include("../class/mpconn.class.php");
	include("../class/debaser.class.php");
	include("../inc/setConn.php");
	
	$conn = $dbs->getDBConn($id);
?>
<table border="0" cellpadding="2" cellspacing="0">
	<tr id="headbar">
		<td class="tablename"><?php echo($tab); ?>&nbsp;&nbsp;<a href="#" title="new" class="branco" onclick="openInsert(<?php echo($id); ?>, '<?php echo($tab); ?>')">[+]</a></td>
		<td class="tablename" align="right"><img src="im/ref.gif" alt="Refresh" onclick="openData(<?php echo($id); ?>, '<?php echo($tab); ?>')">&nbsp;<img src="im/clear.gif" border="0" onclick="closeWin('<?php echo($winId); ?>')"></td>
	</tr>
	<tr>
		<td valign="top" colspan="2">
			<table border="0" cellpadding="2" cellspacing="0">
				<tr style="background-color: green;">
					<td>&nbsp;|&nbsp;</td>
					<?php
						$flds = $conn->getFields($tab);
						while($rowF = mysqli_fetch_array($flds)) {
							echo("<th class=\"branco\">&nbsp;" . $rowF[0] . "&nbsp;</th>");
							echo("<td class=\"branco\">&nbsp;|&nbsp;</td>");
						}
					?>
				</tr>
				<?php
					$sql = "SELECT * FROM $tab";
					
					$rsData = $conn->getData($sql);
					
					$r = mysqli_num_fields($rsData);
					
					$cor1 = '66B8FB';
					$cor2 = '99D0FD';
					
					$cor = $cor1;
			
				$numR = 0;
				while($row = mysqli_fetch_array($rsData)) {
				echo("<tr bgcolor='#$cor'>");
				echo("<td valign='top' align='center'>|</td>");
				for ($i = 0; $i < $r; $i++) {
					echo("<td valign='top' nowrap='nowrap'>");
					
					$tdata = $row[$i];
					$tdata = str_replace(PHP_EOL, "", $tdata);
					$tdata = htmlentities($tdata, ENT_QUOTES);
					
					?>
					<div id="v<?php echo(mysqli_field_name($rsData, $i)); ?>_<?php echo($numR); ?>" style="display: inline;">
						<?php echo($tdata); ?>
					</div>
					<?php
					echo("</td><td valign='top' align='center'>|</td>");
					
					++$numR;
				}
				
				echo("</tr>");
				if ($cor == $cor1) {
					$cor = $cor2;
				} else {
					$cor = $cor1;
				}
			}
			$r *= 2;
			$r++;
			?>
			</table>
		</td>
	</tr>
</table>