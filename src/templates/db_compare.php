<?php
	require('../_def.php');
	
	$winId = $httpget->getString('winId');
	
	$databases = $dbs->getDBs();
?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr id="headbar">
	  <td colspan="2"><table width="100%" border="0" cellpadding="2" cellspacing="0">
        <tr>
          <td class="tablename">Compare databases</td>
		  <td class="tablename" align="right">
		  	<a href="#" onclick="showDBCompareWin(); return false;" title="refresh"><img src="im/ref.gif" alt="" /></a>
			<a href="#" onclick="closeWin('<?php echo($winId); ?>'); return false;" title="close"><img src="im/clear.gif" border="0" alt="" /></a>
		  </td>
        </tr>
      </table></td>
	</tr>
	<tr>
		<td colspan="2">
			
			<form action="/db_compare.php" method="post" target="_blank">
				<table class="formTable">
					<tr>
						<td>
							<table>
								<tr>
									<th colspan="2" bgcolor="#CCCCCC">database 1</th>
								</tr>
								<tr>
									<td>server</td>
									<td><input type="text" name="server1" id="server1"></td>
								</tr>
								<tr>
									<td>username</td>
									<td><input type="text" name="usern1" id="usern1"></td></tr>
								<tr>
									<td>password</td>
									<td><input type="password" name="passw1" id="passw1"></td>
								</tr>
								<tr>
									<td>database</td>
									<td><input type="text" name="db1" id="db1"></td>
								</tr>
							</table>
						</td>
						<td>
							<table>
								<tr>
									<th colspan="2" bgcolor="#CCCCCC">database 2</th>
								</tr>
								<tr>
									<td>server</td>
									<td><input type="text" name="server2" id="server2"></td>
								</tr>
								<tr>
									<td>username</td>
									<td><input type="text" name="usern2" id="usern2"></td></tr>
								<tr>
									<td>password</td>
									<td><input type="password" name="passw2" id="passw2"></td>
								</tr>
								<tr>
									<td>database</td>
									<td><input type="text" name="db2" id="db2"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit" value="compare"></td>
					</tr>
				</table>
			</form>
			
	    </td>
	</tr>
</table>
