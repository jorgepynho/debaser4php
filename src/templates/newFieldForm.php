<?php
	require('../_def.php');
	
	$iddb = $httpget->getString('iddb');
	$winId = $httpget->getString('winId');
	
	$conn = $dbs->getDBConn($iddb);
	
	$tab = $httpget->getString('ptb'); //nome da tabela
?>
<form action="cmd/createField.php" method="post" onsubmit="return createTableField(this)">
  <table border="0" cellpadding="4" cellspacing="0" class="formTable">
    <tr id="headbar">
      <td colspan="2" class="tablename"><table width="100%" border="0" cellpadding="2" cellspacing="0">
          <tr>
            <td>create new field</td>
            <td align="right"><a href="#" onclick="closeWin('<?php echo($winId); ?>'); return false;"><img src="im/clear.gif" border="0" alt="" /></a></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td>name</td>
      <td><input name="fieldName" type="text" id="fieldName" maxlength="31"></td>
    </tr>
    <tr>
      <td>type</td>
      <td><input name="dataType" type="text" id="dataType" maxlength="31"></td>
    </tr>
    <tr>
      <td>null</td>
      <td><input name="chkNotNull" type="checkbox" id="chkNotNull" value="1"></td>
    </tr>
    <tr>
      <td>default</td>
      <td><input name="defaultValue" type="text" id="default"></td>
    </tr>
    <tr>
      <td>after</td>
      <td>      <input name="after" type="text" id="after"></td>
    </tr>
    <tr>
      <td><input name="tab" type="hidden" id="tab" value="<?php echo $tab; ?>">
      <input name="iddb" type="hidden" id="iddb" value="<?php echo($iddb); ?>" /></td>
      <td><input name="cmdCreate" type="button" id="cmdCreate" value="Create" onclick="doCreateNewField(this.form)"></td>
    </tr>
  </table>
</form>
