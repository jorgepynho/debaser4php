<?php
	require('../_def.php');
	$winId = $httpget->getString('winId', 0);
	$databases = $dbs->getDBs();
?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr id="headbar">
	  <td colspan="2"><table width="100%" border="0" cellpadding="2" cellspacing="0">
        <tr>
          <td class="tablename">New Connection</td>
		  <td class="tablename" align="right">
		  	<a href="#" onclick="showNewConnList(); return false;" title="refresh"><img src="im/ref.gif" alt="" /></a>
			<a href="#" onclick="closeWin('<?php echo($winId); ?>'); return false;"><img src="im/clear.gif" border="0" alt="" /></a>
			</td>
        </tr>
      </table></td>
	</tr>
	<tr><td colspan="2">
		<form action="#" onSubmit="newConnFormSubmit(this); return false;">
			<table class="formTable">
				<tr>
					<td>server</td>
					<td><input type="text" name="server" id="server"></td>
				</tr>
				<tr>
					<td>username</td>
					<td><input type="text" name="usern" id="usern"></td></tr>
				<tr>
					<td>password</td>
					<td><input type="text" name="passw" id="passw"></td>
				</tr>
				<tr>
					<td>database</td>
					<td><input type="text" name="db" id="db"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" value="create"></td>
				</tr>
				</table>
		</form>
	</td></tr>
</table>
