<?php
	require('../_def.php');
	
	$id = $httpget->getString('iddb'); // IDDB
	
	$conn = $dbs->getDBConn($id);
	
	$tablename = $httpget->getString('tableName');
	$pkField = $httpget->getString('pkField');
	
	$sql = "DROP TABLE `$tablename` ";
	
	$conn->execute($sql);
	
	$resp = $conn->getLastErr();
	
	if (strlen($resp) > 0) {
		echo("ERR: $resp");
	} else {
		echo("table droped.");
	}
	
	$conn->close();
