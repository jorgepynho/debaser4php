<?php
	require('../_def.php');
	
	$id = $httpget->getString('iddb'); // IDDB
	
	$conn = $dbs->getDBConn($id);
	
	$tablename = $httpget->getString('tableName', 0);
	$pkField = $httpget->getString('pkField', 0);
	
	$sql = "CREATE TABLE `$tablename` (`$pkField` INTEGER UNSIGNED AUTO_INCREMENT, PRIMARY KEY(`$pkField`)) ENGINE = MYISAM";
	
	$conn->execute($sql);
	
	$resp = $conn->getLastErr();
	
	if (strlen($resp) > 0) {
		echo("ERR: $resp");
	} else {
		echo("table created.");
	}
	
	$conn->close();
