<?php
	require('../_def.php');

	$iddb = $httpget->getString('iddb');

	$conn = $dbs->getDBConn($iddb);

	$create_list = $conn->getAllTablesCreateInfo();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>Debaser Home - Dump schema</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<div>
    	<pre><?php
			echo 'use ' . $conn->database . ';' . PHP_EOL . PHP_EOL;

			foreach($create_list as $c) {
				echo $c . "" . PHP_EOL . PHP_EOL;
			}
		?>
        </pre>
    </div>
</body>
</html>
