<?php
	require('../_def.php');

	$iddb = $httpget->getString('iddb');

	$conn = $dbs->getDBConn($iddb);

	$tables = $conn->getTables();
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>Debaser Home - Reset tables</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<div>
    	<pre><?php
			echo 'use ' . $conn->database . ';' . PHP_EOL . PHP_EOL;
			
			while($row = mysqli_fetch_array($tables)) {
				$tb = $row[0];

				echo "TRUNCATE " . $tb . ";" . PHP_EOL . PHP_EOL;
			}
		?>
        </pre>
    </div>
</body>
</html>
