<?php
	require('../_def.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>Debaser - GEN JAVA</title>
		<link href="../styles.css" rel="stylesheet" type="text/css">
		<link href="gen_styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<?php
			$id = $httppost->getString('hIDDB');
			
			$conn = $dbs->getDBConn($id);
			
			$tb = $httppost->getString('lstTable');
		?><br>
        tabela: <strong><?php echo($tb); ?></strong><br>
        <br><br>
       	
		<div class="midTtitle">Bean: <strong><?php echo($tb); ?>.java</strong></div>
        <textarea name="txtJavaBean" cols="100" rows="20" id="txtJavaBean">
		
public class <?php echo($tb); ?> {
<?php
$flds = $conn->getFields($tb);

foreach($flds as $f) {
	echo("\tprivate String " . $f . ";\n");
}
?>

	public <?php echo($tb); ?>() {
		init();
	}
	
	private void init() {
<?php
$flds = $conn->getFields($tb);
foreach($flds as $f) {
	echo("\t\t" . $f . " = \"\";\n");
}
?>
	}
}
		</textarea>
	</body>
</html>
