<?php
	require('../_def.php');
	
	/* 
		faz o echo com n tabs e termina com \r\n
		$str a string a escrever
		$t o n�mero de tabs antes
		$nl o n�mero de \r\n depois
	*/
	function echotnl($str = "", $t = 0, $nl = 0) {
		echo(str_repeat("\t", $t));
		echo($str);
		echo(str_repeat("\r\n", $nl));
	}
	
	function naming($n) {
		$n = ucfirst($n);
		$n = str_replace("_", " ", $n);
		$n = ucwords($n);
		$n = str_replace(" ", "", $n);
		
		return $n;
	}
	
	function getter($n) {
		return "get" . naming($n);
	}
	
	function setter($n) {
		return "set" . naming($n);
	}
	
	$id = $httppost->getString('hIDDB', 1);
	$tb = $httppost->getString('lstTable', 1);
	
	$conn = $dbs->getDBConn($id);
	
	$tb_lang = $tb . '_lang';
	if (!$conn->isTable($tb_lang)) {
		$tb_lang = '';
	}

	$pk = '';

	$flds = $conn->getPKFields($tb);

	if ($flds) $pk = $flds[0];

	$tbs_like = array();
	$flds = $conn->getTables($tb . '%');
	while($f = mysqli_fetch_assoc($flds)) {
		//echo $f['TABLE_NAME'] . "<br />" . PHP_EOL;
		
		$tbs_like[] = $f['TABLE_NAME'];
	}
	
	$ff = $conn->getFirstField($tb);
	
	if (!$pk) $pk = $ff;
	
	//print_r($tbs_like);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>Debaser - GEN PHP - <?php echo $tb; ?></title>
		<link href="../styles.css" rel="stylesheet" type="text/css">
		<link href="gen_styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<br>
        tabela: <strong><?php echo($tb); ?></strong><br>
        <br><br>

		<div class="midTtitle">class: <strong><?php echo($tb); ?>.class.php (v1)</strong></div>
		<textarea name="txtJavaBean1" id="txtJavaBean1">
	class <?php echo($tb); ?> {
<?php
			$flds = $conn->getFields($tb);

			//var_dump($flds);

			foreach($flds as $f) {
				echo("\t\tpublic $" . $f . ";" . PHP_EOL);
			}
		?>
		
		public function __construct() {
			$this->init();
		}
		
		private function init() {
	<?php
		$flds = $conn->getFields($tb);
		foreach($flds as $f) {
			echo("\t\t\$this->" . $f . " = '';\n\t");
		}
	?>	}
	}
		</textarea>

		<div class="midTtitle">class: <strong><?php echo($tb); ?>.class.php (v2)</strong></div>
        <textarea name="txtJavaBean" id="txtJavaBean">
	class <?php echo($tb); ?> {
		<?php
			$flds = $conn->getFields($tb);

			//var_dump($flds);

			foreach($flds as $f) {
				echo("private $" . $f . ";" . PHP_EOL);
			}
		?>
		
		public function __construct() {
		}
		
		private function init() {
	<?php
		$flds = $conn->getFields($tb);
		foreach($flds as $f) {
			echo("\t\t\$this->" . $f . " = '';\n\t");
		}
	?>	}
<?php
		$flds = $conn->getFields($tb);
		foreach($flds as $f) {

			$naming = naming($f);
			
			echo("\t\tpublic function get" . $naming . "() {\n");
			echo("\t\t\treturn \$this->" . $f . ";" .PHP_EOL);
			echo("\t\t}" . PHP_EOL);
			echo("\t\tpublic function set" . $naming . "(\$val) {\n");
			echo("\t\t\t\$this->" . $f . " = \$val;" .PHP_EOL);
			echo("\t\t}\n\n");
		}
		?>
	}
		</textarea>
		
		<div class="midTtitle">vars</div>
		<textarea><?php
		
			$flds = $conn->getFieldsInfo($tb);
			while($f = $flds->fetch_assoc()) {
				
				//print_r($f);
				
				echo("$" . $f['COLUMN_NAME'] . " = ");

				if ($conn->isNumericType($f['DATA_TYPE'])) {
					echo("0");
				} else {
					echo("''");
				}

				echo(";" . PHP_EOL);
			}
			
			echo PHP_EOL;
			
			if ($tb_lang) {
				
				$subflds = $conn->getFields($tb_lang);
				while($sf = mysqli_fetch_assoc($subflds)) {
					
					if ($pk == $sf['Field']) continue;
					
					echo '$' . $sf['Field'] . " = array();" . PHP_EOL;
				}
				
				echo PHP_EOL;
				echo 'foreach($langs as $l) {' . PHP_EOL;
				
				$subflds = $conn->getFields($tb_lang);
				while($sf = mysqli_fetch_assoc($subflds)) {
					
					if ($pk == $sf['Field']) continue;
					
					echo "\t" . '$' . $sf['Field'] . '[$l-&gt;lang] = ';
					
					if ($conn->isNumericType($sf['Type'])) {
						echo("0");
					} else {
						echo("''");
					}
					
					echo(";" . PHP_EOL);
					
					//echo('$categoria[$l-&gt;lang] = \'\';');
				}
				
				echo '}' . PHP_EOL;
			}
		?>
		</textarea>
		
		<div class="midTtitle">var 2</div>
		<textarea><?php
		
			$flds = $conn->getFieldsInfo($tb);
			while($f = $flds->fetch_assoc()) {
				echo("if (!isset(\$" . $f['COLUMN_NAME'] . ")) {\$" . $f['COLUMN_NAME'] . " = ");
				
				if ($conn->isNumericType($f['DATA_TYPE'])) {
					echo("0");
				} else {
					echo("''");
				}
				
				echo(";}" . PHP_EOL);
			}
		?>
		</textarea>
		
		<div class="midTtitle">posts</div>
		<textarea><?php
		
			$flds = $conn->getFieldsInfo($tb);
			while($f = $flds->fetch_assoc()) {
				echo("$" . $f['COLUMN_NAME'] . " = \$httppost->get");
				
				if ($conn->isNumericType($f['DATA_TYPE'])) {
					echo("Int");
				} else {
					echo("String");
				}
				
				echo("(\"" . $f['COLUMN_NAME'] . "\");" . PHP_EOL);
			}
			
			if ($tb_lang) {
				
				echo PHP_EOL;
				
				$subflds = $conn->getFieldsInfo($tb_lang);
				while($sf = $subflds->fetch_assoc()) {
					
					if ($pk == $sf['COLUMN_NAME']) continue;
					
					echo '$' . $sf['COLUMN_NAME'] . " = array();" . PHP_EOL;
				}
				
				echo PHP_EOL;
				echo 'foreach($langs as $l) {' . PHP_EOL;
				
				$subflds = $conn->getFields($tb_lang);
				while($sf = mysqli_fetch_assoc($subflds)) {
					
					if ($pk == $sf['Field']) continue;
					
					echo "\t" . '$' . $sf['Field'] . '[$l-&gt;lang] = $httppost->get';
					
					if ($conn->isNumericType($sf['Type'])) {
						echo("Int");
					} else {
						echo("String");
					}
					
					echo("('" . $sf['Field'] . "_' . \$l-&gt;lang);" . PHP_EOL);
					
					//echo('$categoria[$l-&gt;lang] = \'\';');
				}
				
				echo PHP_EOL;
				
				$subflds = $conn->getFields($tb_lang);
				while($sf = mysqli_fetch_assoc($subflds)) {
					
					if ($pk == $sf['Field']) continue;
					
					echo "\t" . '$' . $sf['Field'] . '[$l-&gt;lang] = $conn->escape';
					
					echo("($" . $sf['Field'] . "[\$l-&gt;lang]);" . PHP_EOL);
					
					//echo('$categoria[$l-&gt;lang] = \'\';');
				}
				
				echo '}' . PHP_EOL;
			}
			
		?>
		</textarea>
		
		<div class="midTtitle">posts 2</div>
		<textarea><?php
			
			$flds = $conn->getFieldsInfo($tb);
			while($f = $flds->fetch_assoc()) {
				echo("\$" . $f['COLUMN_NAME'] . " = isset(\$_POST['" . $f['COLUMN_NAME'] . "']) ? \$_POST['" . $f['COLUMN_NAME'] . "'] : '';" . PHP_EOL);
			}
		?>
		</textarea>
		
		<div class="midTtitle">getObject</div>
		<textarea><?php

			$pkf = $conn->getPKFields($tb);

			if ($pkf) {
				foreach($pkf as $f) {
					$idf = strtolower(substr($f, 0, 3));
					echo('$' . $f . " = \$httpget->getInt('id');" . PHP_EOL);
				}
			} else {
				echo('$' . $ff . " = \$httpget->getInt('id');" . PHP_EOL);
			}
			
			echo(PHP_EOL);
			
			echo('$sql = "SELECT * FROM ' . $tb . ' WHERE');

			if ($pkf) {
				foreach($pkf as $f) {
					$idf = strtolower(substr($f, 0, 3));
					echo(' ' . $f . ' = $' . $pk . '";');
				}
			} else {
				echo(' ' . $ff . ' = $' . $pk . '";');
			}
			
			echo(PHP_EOL);
			
			echo("\$obj_" . $tb . " = \$conn->getObject(\$sql);");
			
			echo(PHP_EOL);
			echo(PHP_EOL);
			
			$npkf = $conn->getNonPKFields($tb);
			foreach($npkf as $f) {
				
				if ($f == $pk) continue;
				
				echo("\$" . $f . " = \$obj_" . $tb . "->" . $f . ";" . PHP_EOL);
			}
			
			if ($tb_lang) {
				
				echo PHP_EOL;
				
				echo('$sql = "SELECT * FROM ' . $tb_lang . ' WHERE ' . $pk . ' = $' . $pk . '";' . PHP_EOL);
				echo('$rs_' . $tb_lang . ' = $conn->getData($sql);' . PHP_EOL);
				
				echo PHP_EOL;

				$subflds = $conn->getNonPKFields($tb_lang);
				while($sf = mysqli_fetch_array($subflds)) {
					
					if ($sf['Field'] == $pk) continue;
					if ($sf['Field'] == 'lang') continue;
					
					//echo('$' . $sf['Field'] . ' = $obj_' . $tb_lang . '->' . $sf['Field'] . ';' . PHP_EOL);
					echo('$' . $sf['Field'] . ' = array();' . PHP_EOL);
				}
				
				echo PHP_EOL;
				
				echo 'foreach($langs as $l) {' . PHP_EOL;
				
				$subflds = $conn->getNonPKFields($tb_lang);
				while($sf = mysqli_fetch_array($subflds)) {
					
					if ($sf['Field'] == $pk) continue;
					if ($sf['Field'] == 'lang') continue;
					
					//echo('$' . $sf['Field'] . ' = $obj_' . $tb_lang . '->' . $sf['Field'] . ';' . PHP_EOL);
					echo("\t" . '$' . $sf['Field'] . '[$l->lang] = \'\';' . PHP_EOL);
				}
				
				echo "}" . PHP_EOL;
				
				echo PHP_EOL;
				
				echo 'while($row = $rs_' .$tb_lang  . '->fetch_object()) {' . PHP_EOL;
				
				$subflds = $conn->getNonPKFields($tb_lang);
				while($sf = mysqli_fetch_array($subflds)) {
					
					if ($sf['Field'] == $pk) continue;
					if ($sf['Field'] == 'lang') continue;
					
					//echo('$' . $sf['Field'] . ' = $obj_' . $tb_lang . '->' . $sf['Field'] . ';' . PHP_EOL);
					echo("\t" . '$' . $sf['Field'] . '[$row->lang] = $row->' . $sf['Field'] . ';' . PHP_EOL);
				}
				
				echo "}" . PHP_EOL;
			}
			
			if ($tbs_like) {
				foreach($tbs_like as $tbl) {
					echo PHP_EOL;
					
					echo('$sql = "SELECT * FROM ' . $tbl . ' WHERE ' . $pk . ' = $' . $pk . '";' . PHP_EOL);
					echo('$lista_' . $tbl . ' = $conn->getData($sql);' . PHP_EOL);
				}
			}
		?>
		</textarea>
		
		<div class="midTtitle">HTML Form (table)</div>
		<textarea><?php
			
			echo("<form name=\"form_$tb\" action=\"/manager/index.php<?php echo(\$form_action); ?>\" method=\"post\">" . PHP_EOL);
			echo("<table class=\"formTable\">" . PHP_EOL);
			
			$flds = $conn->getPKFields($tb);
			foreach($flds as $f) {
				
				echotnl('<?php if ($' . $f . ' > 0) { ?>', 1, 1);
				
				echotnl("<tr>", 1, 1);
					echotnl("<td><label for=\"input_" . $f . "\">", 2, 0);
					echotnl($f, 0, 0);
					echotnl("</label></td>", 0, 1);
					
					echotnl("<td>", 2, 0);
					echotnl("<input type=\"text\" id=\"input_" . $f . "\" name=\"" . $f . "\" value=\"<?php echo($" . $f . "); ?>\" size=\"5\" readonly=\"readonly\" />", 0, 0);
					echotnl("</td>", 0, 1);
				echotnl("</tr>", 1, 1);
				
				echotnl('<?php } ?>', 1, 1);
			}
			
			$flds = $conn->getNonPKFields($tb);
			foreach($flds as $f) {
				
				echotnl("<tr>", 1, 1);
					echotnl("<td><label for=\"input_" . $f[0] . "\">", 2, 0);
					echotnl($f[0], 0, 0);
					echotnl("</label></td>", 0, 1);
					
					echotnl("<td>", 2, 0);
					
					if ($conn->isFieldBigText($tb, $f[0])) {
						echotnl("&lt;textarea id=\"input_" . $f[0] . "\" name=\"" . $f[0] . "\"><?php echo($" . $f[0] . "); ?>&lt;/textarea>", 0, 0);
					} else {
						echotnl("<input type=\"text\" id=\"input_" . $f[0] . "\" name=\"" . $f[0] . "\" value=\"<?php echo($" . $f[0] . "); ?>\" />", 0, 0);
					}
					
					echotnl("</td>", 0, 1);
				echotnl("</tr>", 1, 1);
			}
			
			if ($tb_lang) {
				?>
		<tr>
		  <td>&amp;nbsp;</td>
		  <td>

 &lt;?php foreach($langs as $l) { ?&gt;
&lt;fieldset&gt;
&lt;legend&gt;&amp;nbsp;&lt;?php echo $l-&gt;nome; ?&gt;&amp;nbsp;&amp;nbsp;&lt;/legend&gt;
&lt;table class=&quot;formTable&quot;&gt;
<?php
	$subflds = $conn->getPKFields($tb_lang);
	while(false && $sf = mysqli_fetch_assoc($subflds)) { //ignorar
?>
&lt;tr&gt;
	&lt;td&gt;&lt;label for=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot;&gt;<?php echo $sf['Field'] ?>&lt;/label&gt;&lt;/td&gt;
	&lt;td&gt;&lt;input type=&quot;text&quot; id=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; name=&quot;<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; value=&quot;&lt;?php echo($<?php echo $sf['Field'] ?>[$l-&gt;lang]); ?&gt;&quot; size=&quot;50&quot; /&gt;&lt;/td&gt;
&lt;/tr&gt;
<?php
	}
	
	$subflds = $conn->getNonPKFields($tb_lang);
	while($sf = mysqli_fetch_assoc($subflds)) {
		
		if ($pk == $sf['Field']) continue;
		if ('lang' == $sf['Field']) continue;
?>
&lt;tr&gt;
	&lt;td&gt;&lt;label for=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot;&gt;<?php echo $sf['Field'] ?>&lt;/label&gt;&lt;/td&gt;
	
	&lt;td&gt;
	<?php if ($conn->isFieldBigText($tb_lang, $sf['Field'])) { ?>
		&lt;textarea name=&quot;<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; id=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot;&gt;&lt;?php echo($<?php echo $sf['Field'] ?>[$l-&gt;lang]); ?&gt;&lt;/textarea&gt;
	<?php } else { ?>
		&lt;input type=&quot;text&quot; id=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; name=&quot;<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; value=&quot;&lt;?php echo($<?php echo $sf['Field'] ?>[$l-&gt;lang]); ?&gt;&quot; size=&quot;50&quot; /&gt;
	<?php } ?>
	&lt;/td&gt;
&lt;/tr&gt;
<?php } ?>
&lt;/table&gt;

&lt;/fieldset&gt;&lt;br&gt;
&lt;?php } ?&gt;

		  </td>
	  </tr>
				<?php
			}
			
			?>
		<tr>
		  <td>&amp;nbsp;</td>
		  <td>&amp;nbsp;</td>
	  </tr>
		<tr>
		  <td>&amp;nbsp;</td>
	      <td><input type="submit" value="Gravar" />
          <input type="reset" value="Rep�r" /></td>
	  </tr>
			<?php
			echo("</table>\r\n");
			echo("</form>\r\n");
		?>
		</textarea>		
		
		<div class="midTtitle">HTML Form (div)</div>
		<textarea><?php
			
			echo("<form name=\"form_$tb\" action=\"<?php echo(\$_SERVER['SCRIPT_NAME']); ?>\" method=\"post\">" . PHP_EOL);
			echo("<div class=\"form-box\">" . PHP_EOL);
			
			$flds = $conn->getPKFields($tb);
			foreach($flds as $f) {
				
				echotnl('<?php if ($' . $f . ' > 0) { ?>', 1, 1);
				
				echotnl("<div class=\"form-field\">", 1, 1);
					echotnl("<label for=\"input_" . $f . "\">", 2, 0);
					echotnl($f, 0, 0);
					echotnl("</label><br>", 0, 1);
					
					echotnl("<input type=\"text\" id=\"input_" . $f . "\" name=\"" . $f . "\" value=\"<?php echo($" . $f . "); ?>\" size=\"5\" readonly=\"readonly\" />", 2, 1);
				echotnl("</div><br>", 1, 1);
				
				echotnl('<?php } ?>', 1, 1);
			}
			
			$flds = $conn->getNonPKFields($tb);
			foreach($flds as $f) {
				
				echotnl("<div class=\"form-field\">", 1, 1);
					echotnl("<label for=\"input_" . $f . "\">", 2, 0);
					echotnl($f, 0, 0);
					echotnl("</label><br>", 0, 1);
					
					echotnl("<input type=\"text\" id=\"input_" . $f . "\" name=\"" . $f . "\" value=\"<?php echo($" . $f . "); ?>\" />", 2, 1);
				echotnl("</div><br>", 1, 1);
			}
			
			if ($tb_lang) {
				?>
		<tr>
		  <td>&amp;nbsp;</td>
		  <td>

 &lt;?php foreach($langs as $l) { ?&gt;
&lt;fieldset&gt;
&lt;legend&gt;&amp;nbsp;&lt;?php echo $l-&gt;nome; ?&gt;&amp;nbsp;&amp;nbsp;&lt;/legend&gt;
&lt;table class=&quot;formTable&quot;&gt;
<?php
	$subflds = $conn->getPKFields($tb_lang);
	while(false && $sf = mysqli_fetch_assoc($subflds)) { //ignorar
?>
&lt;tr&gt;
	&lt;td&gt;&lt;label for=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot;&gt;<?php echo $sf['Field'] ?>&lt;/label&gt;&lt;/td&gt;
	&lt;td&gt;&lt;input type=&quot;text&quot; id=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; name=&quot;<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; value=&quot;&lt;?php echo($<?php echo $sf['Field'] ?>[$l-&gt;lang]); ?&gt;&quot; size=&quot;50&quot; /&gt;&lt;/td&gt;
&lt;/tr&gt;
<?php
	}
	
	$subflds = $conn->getNonPKFields($tb_lang);
	while($sf = mysqli_fetch_assoc($subflds)) {
		
		if ($pk == $sf['Field']) continue;
		if ('lang' == $sf['Field']) continue;
?>
&lt;tr&gt;
	&lt;td&gt;&lt;label for=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot;&gt;<?php echo $sf['Field'] ?>&lt;/label&gt;&lt;/td&gt;
	&lt;td&gt;&lt;input type=&quot;text&quot; id=&quot;input_<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; name=&quot;<?php echo $sf['Field'] ?>_&lt;?php echo $l-&gt;lang; ?&gt;&quot; value=&quot;&lt;?php echo($<?php echo $sf['Field'] ?>[$l-&gt;lang]); ?&gt;&quot; size=&quot;50&quot; /&gt;&lt;/td&gt;
&lt;/tr&gt;
<?php } ?>
&lt;/table&gt;

&lt;/fieldset&gt;&lt;br&gt;
&lt;?php } ?&gt;

		  </td>
	  </tr>
				<?php
			}
			
			?>
	<div class="form-field">
		<input type="submit" value="Gravar" />
		<input type="reset" value="Rep�r" /></td>
	</div>
<?php
			echo("</div>" . PHP_EOL);
			echo("</form>" . PHP_EOL);
		?>
		</textarea>
		
		<div class="midTtitle">HTML detail page</div>
		<textarea>
			<table class="dataTable">
		<?php
			$flds = $conn->getPKFields($tb);
			foreach($flds as $f) {
				?><tr>
					<td><?php echo $f ?></td>
					<td>&lt;?php echo($<?php echo $f ?>); ?&gt;</td>
				</tr>
				<?php
			}
			
			$flds = $conn->getNonPKFields($tb);
			foreach($flds as $f) {
				?><tr>
					<td><?php echo $f ?></td>
					<td>&lt;?php echo($<?php echo $f ?>); ?&gt;</td>
				</tr>
				<?php
			}
			?>
			</table>
			<br>
			
	  
	  <?php if ($tb_lang) { ?>
	  
	  	<table>
	  
	  	<td>&lt;?php
foreach($langs as $l) {

	?&gt;
		<fieldset class="fieldTable">
			<legend>&amp;nbsp;<strong>&lt;?php echo $l->nome; ?&gt;</strong>&amp;nbsp;&amp;nbsp;</legend>
			<table class="dataTable">
		<?php
			$subflds = $conn->getNonPKFields($tb_lang);
			while($sf = mysqli_fetch_assoc($subflds)) {
				
				if ($pk == $sf['Field']) continue;
				if ('lang' == $sf['Field']) continue;
				?>
					<tr>
						<td><?php echo $sf['Field']; ?></td>
						<td>&lt;?php echo $<?php echo $sf['Field']; ?>[$l->lang]; ?&gt;</td>
					</tr>
				<?php
			}
		?>
			</table>
		</fieldset><br>
	&lt;?php
}
?&gt;</td>
	  </table>
	  <?php } ?>
	  
	  <?php
	  	if ($tbs_like) {
			
			echo "<br>" . PHP_EOL;
			
			foreach($tbs_like as $tl) {
				echo $tl . "<br>" . PHP_EOL;
				?>
					<table class="dataTable">
						<thead>
							<tr>
							<?php
								$flds = $conn->getFields($tl);
								while($f = mysqli_fetch_assoc($flds)) {
									
									if ($pk == $f['Field']) continue;
									
									?><th><?php echo $f['Field']; ?></th>
									<?php
								}
							?>
							</tr>
						</thead>
						<tfoot></tfoot>
						<tbody>
						
	&lt;?php
	$sql = "SELECT * FROM <?php echo($tl); ?>";
	$tabData = $conn->getData($sql);
	while($obj = $tabData->fetch_object()) {
		?&gt;
			<tr>
			
										<?php
								$flds = $conn->getFields($tl);
								while($f = mysqli_fetch_assoc($flds)) {
									
									if ($pk == $f['Field']) continue;
									
									?><td>&lt;?php echo $obj-><?php echo $f['Field']; ?>; ?&gt;</th>
									<?php
								}
							?>
			
			</tr>
		&lt;?php } ?&gt;
		
						</tbody>
					</table>
					<br>
				<?php
			}
		?>
	  <?php } ?>
		</textarea>
		
<div class="midTtitle">HTML list page</div>
<textarea>
	<a href="/manager/index.php/<?php echo $tb ?>/novo/form">novo</a><br><br>
	&lt;?php
	$sql = "SELECT * FROM <?php echo($tb); ?>";
	$tabData = $conn->getData($sql);
	?&gt;
	<table class="genTableList">
		<thead>
		<tr>
	
	<?php	
			$flds = $conn->getPKFields($tb);
			foreach($flds as $f) {
				echo("<th>" . $f . "</th>" . PHP_EOL);
			}
			
			$flds = $conn->getNonPKFields($tb);
			foreach($flds as $f) {
				echo("<th>" . $f . "</th>" . PHP_EOL);
			}
			
			if ($tb_lang) {
				?><th>&amp;nbsp;</th><?php
			}
	?>
		<th>&amp;nbsp;</th>
		<th>&amp;nbsp;</th>
	</tr>
	</thead>
	<tfoot></tfoot>
	<tbody>
	&lt;?php
	while($obj = $tabData->fetch_object()) {
		?&gt;
		<tr>
		<?php
			$flds = $conn->getPKFields($tb);
			foreach($flds as $f) {
				?>	<td>&lt;?php echo($obj-><?php echo($f); ?>); ?&gt;</td>
			<?php
			}
			
			$field_count = count($flds);

			$flds = $conn->getNonPKFields($tb);
			foreach($flds as $f) {
			?><td>&lt;?php echo($obj-><?php echo($f[0]); ?>); ?&gt;</td>
			<?php
			}
			
			$field_count += count($flds);
			
			if ($tb_lang) {
				?><td><table class="subTable">
					<thead>
						<tr>
							<?php
								$subflds = $conn->getPKFields($tb_lang);
								while(false && $sf = mysqli_fetch_assoc($subflds)) {
									if ($sf['Field'] == $pk) continue;
									echo("<th>" . $sf['Field'] . "</th>" . PHP_EOL);
								}
								
								$subflds = $conn->getNonPKFields($tb_lang);
								while($sf = mysqli_fetch_assoc($subflds)) {
									if ($sf['Field'] == $pk) continue;
									echo("<th>" . $sf['Field'] . "</th>" . PHP_EOL);
								}
							?>
						</tr>
					</thead>
					<tfoot></tfoot>
					<tbody>
							&lt;?php
							
							$sql = "SELECT * FROM <?php echo $tb_lang; ?> WHERE <?php echo $pk; ?> = $obj-><?php echo $pk; ?>";
							$subData = $conn->getData($sql);
	while($subobj = $subData->fetch_object()) {
		?&gt;
		<tr>
			<?php
			$subflds = $conn->getPKFields($tb_lang);
			while(false && $sf = mysqli_fetch_assoc($subflds)) {
				
				if ($sf['Field'] == $pk) continue;
				
				?>	<td>&lt;?php echo($subobj-><?php echo($sf['Field']); ?>); ?&gt;</td>
			<?php
			}
			
			$subflds = $conn->getNonPKFields($tb_lang);
			while($sf = mysqli_fetch_assoc($subflds)) {
				
				if ($sf['Field'] == $pk) continue;
				
			?><td>&lt;?php echo($subobj-><?php echo($sf['Field']); ?>); ?&gt;</td>
			<?php
			}
			?>
		</tr>
		&lt;?php } ?&gt;
		
					</tbody>
				</table></td><?php
			}
		?>
			<td><a href="/manager/index.php/<?php echo $tb; ?>/editar/form&amp;idc=&lt;?php echo($obj-><?php echo $pk; ?>); ?&gt;" title="editar"><img src="/manager/images/edit.png" alt="editar"></a></td>
			<td><a href="/manager/index.php/<?php echo $tb; ?>/del&amp;idc=&lt;?php echo($obj-><?php echo $pk; ?>); ?&gt;" title="apagar" onclick="return confirm('tem a certeza ?');"><img src="/manager/images/del.png" alt="apagar"></a></td>
		</tr>
		&lt;?php }
		
		if ($tabData->num_rows < 1) {
			?&gt;
			<tr>
				<td colspan="<?php echo ($field_count + 2); ?>">-- lista vazia --</td>
			</tr>
			&lt;?php
		}
		
		?&gt;
		</tbody>
	</table>
</textarea>	

<div class="midTtitle">Form validation</div>
<textarea>
function <?php echo $tb; ?>_validate($vpk = false) {
<?php if ($tb_lang) { ?>
	global $langs;
<?php } ?>
	
	$instance = new FormValidation();
	
	if ($vpk) {
<?php
	$flds = $conn->getPKFields($tb);
	foreach($flds as $f) {
		?>
		$instance-&gt;addRule('<?php echo $f; ?>', 'required', 'An error ocurred.');
<?php
		
	}
?>	}

<?php
	$flds = $conn->getNonPKFields($tb);
	foreach($flds as $f) {
		?>
	$instance-&gt;addRule('<?php echo $f; ?>', 'required', '<?php echo $f; ?> missing');
<?php
	}
	
	if ($tb_lang) { ?>
		foreach($langs as $l) {
		<?php
	$subflds = $conn->getNonPKFields($tb_lang);
	while($sf = mysqli_fetch_assoc($subflds)) {
		
		if ($pk == $sf['Field']) continue;
		if ($sf['Field'] == 'lang') continue;
		?>$instance-&gt;addRule('<?php echo $sf['Field']; ?>_' . $l-&gt;lang, 'required', '<?php echo $sf['Field']; ?> (' . $l-&gt;lang . ') missing');
		<?php } ?>
}
<?php } ?>
	
	return $instance-&gt;validate();
}
</textarea>


<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

	</body>
</html>
