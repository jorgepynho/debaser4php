<?php session_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>Debaser - GEN ASP</title>
		<link href="../styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1 style="color:red;">ATENÇÃO - PORT A DECORRER PARA ASP</h1>
		<?php
			include("../inc/rq.php");
			
			include("../func/date.func.php");
			
			include("../class/mpconn.class.php");
			include("../class/debaser.class.php");
			include("../class/Logs.class.php");
			include("../inc/setConn.php");
			
			/* 
				faz o echo com n tabs e termina com \r\n
				$str a string a escrever
				$t o numero de tabs antes
				$nl o numero de \r\n depois
			*/
			function echotnl($str = "", $t = 0, $nl = 0) {
				echo(str_repeat("\t", $t));
				echo($str);
				echo(str_repeat("\r\n", $nl));
			}
			
			$id = iRQ('hIDDB', 1);
			
			$conn = $dbs->getDBConn($id);
			
			$tb = sRQ('lstTable', 1);
			
			$flds = $conn->getFields($tb);
		?><br>
        tabela: <strong><?php echo($tb); ?></strong><br>
        <br><br>
       	class: <strong><?php echo($tb); ?>.class.php</strong><br>
        <textarea name="txtJavaBean" cols="80" rows="10" id="txtJavaBean">
	class <?php echo($tb); ?> {
		<?php
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echo("var $" . $f[0] . ";\n\t\t");
			}
		?>
		
		function <?php echo($tb); ?>() {
	<?php
		$flds = $conn->getFields($tb);
		while($f = mysqli_fetch_array($flds)) {
			echo("\t\t\$this->" . $f[0] . " = '';\n\t");
		}
	?>	}
		
		/**
		 * recebe um mysq_fetch_object e sincroniza com as mesmas propriedades
		 */
		function sync($rs) {
<?php
		$flds = $conn->getFields($tb);
		while($f = mysqli_fetch_array($flds)) {
			echo("\t\t\t\$" . "this->" . $f[0] . " = $" . "rs->" . $f[0] . ";\n");
		}
	?>
		}
		
<?php
		$flds = $conn->getFields($tb);
		while($f = mysqli_fetch_array($flds)) {
			echo("\t\tfunction get" . $f[0] . "() {\n");
			echo("\t\t\tif (isset(\$_POST['" . $f[0] . "'])) return \$_POST['" . $f[0] . "'];\n");
			echo("\t\t\n");
			echo("\t\t\treturn \"\"\n");
			echo("\t\t}\n\n");
		}
		?>
	}
		</textarea>
		<br />
<br />
		<strong>vars</strong><br />
		<textarea cols="80" rows="10"><?php
		
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echo($f[0] . " = \"\"\r\n");
			}
		?>
		</textarea>
		
		<br />
<br />
		<strong>posts</strong><br />
		<textarea cols="80" rows="10"><?php
		
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echo($f[0] . " = Request.Form(\"" . $f[0] . "\")\r\n");
			}
		?>
		</textarea>
		<br />
<br />
		<strong>HTML Form</strong><br />
		<textarea cols="120" rows="10"><?php
			
			echo("<form name=\"form" . $tb . "\" action=\"#\" method=\"post\">\r\n");
			echo("<table>\r\n");
			
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				//echo("\t<tr><th>" . $f[0] . "</th></tr>\r\n");
			}
			
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echotnl("<tr>", 1, 1);
					echotnl("<td>", 2, 0);
					echotnl($f[0], 0, 0);
					echotnl("</td>", 0, 1);
					
					echotnl("<td>", 2, 0);
					echotnl("<input type=\"text\" name=\"" . $f[0] . "\" value=\"<%=" . $f[0] . " %>\" />", 0, 0);
					echotnl("</td>", 0, 1);
				echotnl("</tr>", 1, 1);
			}
			?>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
		<tr>
		  <td>&nbsp;</td>
	      <td><input name="cmdSubmit" type="submit" id="cmdSubmit" value="Gravar" />
          <input name="cmdReset" type="reset" id="cmdReset" value="Rep�r" /></td>
	  </tr>
			<?php
			echo("</table>\r\n");
			echo("</form>\r\n");
		?>
		</textarea>		
		<br />
<br />
		
<strong>HTML detail page</strong><br />
		<textarea cols="120" rows="10"><?php
			
			echo("<table>\r\n");
			
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				//echo("\t<tr><th>" . $f[0] . "</th></tr>\r\n");
			}
			
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echotnl("<tr>", 1, 1);
					echotnl("<td>", 2, 0);
					echotnl($f[0], 0, 0);
					echotnl("</td>", 0, 1);
					
					echotnl("<td>", 2, 0);
					echotnl("<%=RS(\"" . $f[0] . "\") %>", 0, 0);
					echotnl("</td>", 0, 1);
				echotnl("</tr>", 1, 1);
			}
			echo("</table>\r\n");
		?>
		</textarea>		
		
		<br />
<br />
<strong>HTML list page</strong><br />

<textarea cols="120" rows="10">
	
	&lt;?php
	$sql = "SELECT * FROM <?php echo($tb); ?>";
	//$conn->openConn();
	$tabData = $conn->getData($sql);
	//$conn->closeConn();
	
	echo("<table border=\"1\" cellpadding=\"2\" cellspacing=\"0\">");
	echo("<tr>");
	
	<?php
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echo("Response.Write(\"<th>" . $f[0] . "</th>\")\r\n");
			}
	?>
	echo("</tr>");
	while($obj = mysqli_fetch_object($tabData)) {
		echo("<tr>");
		
		<?php
			$flds = $conn->getFields($tb);
			while($f = mysqli_fetch_array($flds)) {
				echo("\tResponse.Write(\"<td>" . htmlentities("&nbsp") . ";\" & RS(\"" . $f[0] . "\") & \"" . htmlentities("&nbsp") . "</td>\")\r\n");
			}
		?>
		
		echo("</tr>");
	}
	echo("</table>");
	?&gt;
</textarea>
	</body>
</html>
