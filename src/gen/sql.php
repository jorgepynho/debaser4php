<?php
	require('../_def.php');

	$id = $httppost->getString('hIDDB', 1);
	$tb = $httppost->getString('lstTable', 1);
	
	$pk = '';
	
	$conn = $dbs->getDBConn($id);

	$tb_lang = $tb . '_lang';
	if (!$conn->isTable($tb_lang)) {
		$tb_lang = '';
	}
	
	$flds = $conn->getPKFields($tb);

	if ($flds) $pk = $flds[0];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>Debaser - GEN SQL - <?php echo $tb ?></title>
		<link href="../styles.css" rel="stylesheet" type="text/css">
		<link href="gen_styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
        tabela: <strong><?php echo($tb); ?></strong><br>
        <br>
    <br>
<div class="midTtitle">CREATE TABLE command</strong></div>
<textarea name="txtCreateTable" cols="100" rows="10" id="txtCreateTable"><?php echo($conn->getCreateInfo($tb)); ?></textarea> 
<br>
<br>
<div class="midTtitle">SELECT command 1</strong></div>
<textarea name="txtSELECT1" cols="100" rows="3" id="txtSELECT1">SELECT * FROM <?php echo($tb); ?></textarea>
<br>
<br>
<div class="midTtitle">SELECT command 2</strong></div>
<textarea name="txtSELECT2" cols="100" rows="3" id="txtSELECT2">SELECT <?php
$flds = $conn->getFields($tb);
echo implode(", ", $flds);
?> FROM <?php echo($tb); ?></textarea>
<br>
<div class="midTtitle">INSERT command</strong></div>
<textarea name="txtINSERT" cols="100" rows="3" id="txtINSERT">INSERT INTO <?php echo($tb); ?> (<?php
$flds = $conn->getNonPKFields($tb);
echo implode(", ", $flds);
?>) VALUES (<?php
echo implode(", $", $flds);
?>)</textarea>

<div class="midTtitle">INSERT command 2</strong></div>
<textarea name="txtINSERT" cols="100" rows="3" id="txtINSERT">INSERT INTO <?php echo($tb); ?> SET <?php
$flds = $conn->getNonPKFields($tb);
foreach($flds as $f) {
	echo $f . " = '', ";
}
?></textarea>



<?php if ($tb_lang) { ?>
<div class="midTtitle">INSERT command (lang) - PHP</strong></div>
<textarea cols="100" rows="5" id="txtINSERT_lang">
<?php
	$gen = new SQLGen($conn);
	echo $gen->getPHP_INSERT_lang($tb_lang);
?>
</textarea>
<?php } ?>

<div class="midTtitle">UPDATE command</strong></div>
<textarea name="txtUPDATE" cols="100" rows="5" id="txtUPDATE">UPDATE <?php echo($tb); ?> SET <?php
$flds = $conn->getNonPkFields($tb);
foreach($flds as $k => $f) {
	if ($k) echo(", ");
	echo($f . " = '$" . $f . "'");
}
?> WHERE <?php
	$flds = $conn->getPkFields($tb);
	foreach($flds as $k => $f) {
		if ($k) echo(", ");
		echo($f . " = '$" . $f . "'");
	}
?>
</textarea>
<div class="midTtitle">DELETE command</strong></div>
<textarea name="txtDELETE" cols="100" rows="3" id="txtDELETE">DELETE FROM <?php echo($tb); ?> WHERE <?php
	$flds = $conn->getPkFields($tb);
	foreach($flds as $k => $f) {
		if ($k) echo(" AND ");
		echo($f . " = '$" . $f . "'");
	}
?></textarea>
<div class="midTtitle">DELETE ALL DATA</strong></div>
<textarea name="txtDELETEALLDATA" cols="50" rows="3" id="txtDELETEALLDATA">
<?php
$tbs = $conn->getTables();
$b = false;
while($t = mysqli_fetch_array($tbs)) {
	echo("DELETE FROM " . $t[0] . ";\r\n");
}
?>
</textarea>

<div class="midTtitle">DROP ALL TABLES</strong></div>
<textarea name="txtDROPALLTABLES" cols="50" rows="3" id="txtDROPALLTABLES">
<?php
$tbs = $conn->getTables();
$b = false;
while($t = mysqli_fetch_array($tbs)) {
	echo("DROP TABLE " . $t[0] . ";\r\n");
}
?>
</textarea>

<br>
<br>
<br>
<br>
<br>
<br>
	</body>
</html>
