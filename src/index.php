<?php
	require('_def.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Debaser Home</title>
	<meta charset='utf-8'>

	<script src="//code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

	<script type="text/javascript" src="js/log.js"></script>
	<script type="text/javascript" src="js/debaser.js"></script>
	<script type="text/javascript" src="js/dbs-win.js"></script>
	<script type="text/javascript" src="js/mngTable.js"></script>
	<script type="text/javascript" src="js/mngField.js"></script>

	<link type="text/css" rel="stylesheet" href="styles.css">
	<link type="text/css" rel="stylesheet" href="js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
</head>
<body id="bodyNode">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr><td>
			<table border="0" cellpadding="2" cellspacing="0">
			<tr>
			<td><a href="/">Debaser Home</a></td>
			<td><input name="cmdDBs" type="button" id="cmdDBs" value="Connections" onclick="showDBList()"></td>
			<td><input name="cmdDBsComp" type="button" id="cmdDBsComp" value="compare databases" onclick="showDBCompareWin()"></td>
			<!-- td><input name="cmdApps" type="button" id="cmdApps" value="applications" onclick="showAppList()"></td -->
			<td><input name="cmdViewLog" type="button" id="cmdViewLog" value="ajax log Win" onclick="showLogWin()"></td>
			</tr>
			</table>
		</td>
			<td align="right"><a href="https://gitlab.com/jorgepynho/debaser4php" target="_blank">https://gitlab.com/jorgepynho/debaser4php</a> | &copy; JPy 2006 &nbsp;&nbsp;</td>
		</tr>
		<tr><td colspan="2"><img src="im/dot_black.gif" height="2" width="100%" alt=""></td></tr>
	</table>
	
	<div id="wintemplate" class="jdraggable shadow"></div>
	
	<div id="loadAlertWin" class="alertWin">Loading...</div>

	<div id="logWin" class="jdraggable">
		<div id="headbar" class="tablename">Log</div>
		<div id="logBody" class="logWin"></div>
		<input type="button" value="clear" onclick="clearLog();" />
		<input type="button" value="close" onclick="hideLogWin();" />
	</div>
</body>
</html>
