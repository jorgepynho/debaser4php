<?php
	
	/**
	 * Layer for the Logs
	 * 
	 */
	class Logs {
		var $logFile;
		var $format;
		
		/**
		 * Escreve no ficheiro
		 * - %l - Level, DEBUG, ERROR, etc...
		 * - %m - The message to write
		 * - %d - The date
		 * - %h - The time
		 */
		function Logs() {
			$this->format = "%d %t %l %m";
			$this->logFile = false;
		}
		
		function out($level, $msg) {
			$codename = "debaser4php";
			
			$dir = TO_DOC_ROOT . "/../logs";

			$fx = $codename . "." . DateTimeUtils::getNow(1) . ".log";

			if (!$this->logFile) {
				$this->logFile = @fopen("$dir/$fx", "a");
			}

			if (!$this->logFile) {
				return;
			}

			$txt = $this->format;

			$txt = str_replace("%m", $msg, $txt);
			$txt = str_replace("%l", $level, $txt);
			$txt = str_replace("%d", DateTimeUtils::getNow(1), $txt);
			$txt = str_replace("%t", DateTimeUtils::getNow(2), $txt);

			$txt .= "\n";

			fwrite($this->logFile, $txt);
		}
		
		function debug($msg) {
			$this->out("DEBUG", $msg);
		}
		
		function info($msg) {
			$this->out("INFO", $msg);
		}
		
		function warn($msg) {
			$this->out("WARN", $msg);
		}
		
		function error($msg) {
			$this->out("ERROR", $msg);
		}
		
		function closeFile() {
			@fclose($this->logFile);
		}
	}
