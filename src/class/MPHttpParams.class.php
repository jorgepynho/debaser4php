<?php
	
	/**
	 * Utility method to get/set the elements of an array with keys
	 */
	class MPHttpParams {
		
		public $target;
		
		public function __construct(&$t = array()) {
			$this->target = $t;
		}
		
		/**
		 * vai buscar o parametro, se nao existe devolve o default
		 */		
		public function get($p, $def) {
			if (isset($this->target[$p])) {
				return $this->target[$p];
			}
			
			return $def;
		}
		
		/**
		 * This will return as a string
		 */
		public function getString($p, $def = "") {
			return $this->get($p, $def);
		}
		
		/**
		 * This will return the length of the param
		 */
		public function getLength($p) {
			return strlen($this->get($p, ""));
		}
		
		/**
		 * This will return as an int type
		 */
		public function getInt($p, $def = 0) {
			$ret = $this->get($p, $def);
			
			return intval($ret);
		}
		
		/**
		 * This will return as a float type
		 */
		public function getFloat($p) {
			$ret = $this->get($p, 0.0);
			
			return floatval($ret);
		}
		
		/**
		 * This will return as a string with the params separated by commas
		 */
		public function getCSV($p) {
			$prms = $this->getArray($p);
			
			if (is_array($prms)) {
				return join(",", $prms);
			}
			
			return "";
		}
		
		/**
		 * This will return as an array
		 */
		public function getArray($p) {
			return $this->get($p, array());
		}
		
		public function has($p) {
			return isset($this->target[$p]);
		}
		
		public function __toString() {
			return print_r($this->target, true);
		}
	}
