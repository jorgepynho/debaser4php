<?php
	
	/**
	 * Handles the parameters passed by POST
	 */
	class MPHttpPost extends MPHttpParams {
		
		public function __construct() {
			parent::__construct($_POST);
		}
	}
