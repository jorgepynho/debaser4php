<?php

class Var_Dump_Tree_View {

    public static function var_dump_list($obj) {
    	echo '<ul>' . PHP_EOL;
    	
        foreach($obj as $k => $v) {
    		 static::var_dump_item($k, $v);
    	}
    	
    	echo '</ul>' . PHP_EOL;
    }

    public static function var_dump_item($k, $i) {
    	 
    	$type = gettype($i);

    	echo '<li>';
    	echo $k . ' (' . $type . ')';
    	if ($type == 'object') {
    		static::var_dump_list($i);
    	} elseif ($type == 'array') {
    		static::var_dump_list($i);
    	} elseif ($type == 'resource') {
    		//echo ' => [resource]' . PHP_EOL;
    	} else {
    		echo ' => ' . $i . PHP_EOL;
    	}

    	echo '</li>' . PHP_EOL;
    
    }

    public static function var_dump_object($obj) {
    	
    	$type = gettype($obj);

    	if ($type == 'object') {
    		static::var_dump_list($obj);
    	} elseif ($type == 'array') {
    		static::var_dump_list($obj);
    	} elseif ($type == 'resource') {
    		echo '[resource]' . PHP_EOL;
    	} else {
    		echo $obj . PHP_EOL;
    	}
    	
    }

}
