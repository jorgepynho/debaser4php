<?php
	class Debaser extends MPConn {
		
		private $msyql_conns_configs;

		public function __construct() {
			$this->msyql_conns_configs = array();

			$this->load_mysql_conns();
		}

		private function load_mysql_conns() {
			$this->msyql_conns_configs = require TO_DOC_ROOT . '/../config/mysql_conns.config.php';

			foreach ($this->msyql_conns_configs as &$conn_info) {

				try {
					$conn_info['conn'] = new MPConn(
						$conn_info['host'], 
						$conn_info['username'],
						$conn_info['password'], 
						NULL, 
						isset($conn_info['port']) ? $conn_info['port'] : NULL
					);

					$conn_info['dbs'] = $conn_info['conn']->get_databases();
					$conn_info['conn']->close();
					unset($conn_info['conn']);
				} catch(\Throwable $e) {
					$conn_info['dbs'] = [];
					echo 'XXXXXXXXXXXXXXXXXXXXXXx';
					exit;
				}				
			}
		}

		function getDBs() {
			return $this->msyql_conns_configs;
		}

		function newConn($sv, $un, $pw, $db) {
			$sql = "INSERT INTO dbs (Server, dbname, user, pwd) VALUES ('$sv', '$db', '$un', '$pw')";
			$newId = $this->execute($sql);
			
			if ($newId) return 'OK';
			
			return $this->sqlErr;
		}

		function delConn($id) {
			/*$sql = "DELETE FROM dbs WHERE IDDB = $id";
			$this->execute($sql);
			
			$sql = "DELETE FROM apps WHERE IDDB = $id";
			$this->execute($sql);
			
			$sql = "DELETE FROM data_views WHERE iddb = $id";
			$this->execute($sql);
			
			$sql = "DELETE FROM inputs WHERE idDb = $id";
			$this->execute($sql);
			
			$sql = "DELETE FROM table_fields WHERE iddb = $id";
			$this->execute($sql);*/
			
			return true;
		}
		
		/*function getApps() {
			$sql = "SELECT A.idApp, A.Name, D.Server, D.dbname FROM apps A, dbs D WHERE A.IDDB = D.IDDB ORDER BY Name";

			return $this->getData($sql);
		}
		
		function getMenus($ida) {
			$sql = "SELECT * FROM menus WHERE idApp = $ida";
			
			return $this->getData($sql);
		}
		
		function getMenuData($idm) {
			$sql = "SELECT m.sqlCmd FROM menus m WHERE m.idMenu = $idm";
			$sqlCmd = ""; 
			
			$rsSQL = $this->con->getData($sql);
			if ($row = mysqli_fetch_array($rsSQL)) {
				$sqlCmd = $row[0]; 
			}
			
			if (strlen($sqlCmd) < 1) return false;
			
			return $this->appCon->getData($sqlCmd);
		}*/
		
		/**
		 * Escreve a tabela em HTML
		 */
		function writeTable($tb) {
			$fields = $this->getFields($tb);
			echo("\n<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"drag\">");
			echo("\n<tr bgcolor=\"#4073FB\">");
			echo("<td class=\"tablename\">");
			
			echo("<div id=\"div_" . $tb . "\" style=\"position:absolute;left: 50;top: 50;z-index: 3;visibility: visible;width: auto;height: auto;background-color: #ffffff;color:#000000;padding: 2;border-style: solid;border-width: 2;border-color: #0000ff;display:block;\">");
			
			echo($tb);
			echo("</div></td>");
			echo("<td class=\"tablename\" align=\"right\">");
			echo("\n<a href=\"data.php?ptb=$tb\"><img src=\"im/tb.jpg\" title=\"Ver dados\" border=\"0\" alt=\"dados\"></a>");
			echo("\n&nbsp;<a href=\"sql.php?ptb=$tb\"><img src=\"im/sql.jpg\" title=\"Ver SQL\" border=\"0\" alt=\"sql\"></a>");
			echo("\n&nbsp;<a href=\"java.php?ptb=$tb\"><img src=\"im/java.gif\" title=\"Ver JAVA\" border=\"0\" alt=\"java\"></a>");
			echo("</td>");
			echo("</tr>");
			while($fld = mysqli_fetch_array($fields)) {
				echo("\n<tr><td nowrap colspan=\"2\" align=\"left\">");
				echo($fld[0]);
				if ($fld[3] == "PRI") {echo(" <b>PK</b>");}
				echo("</td></tr>");
			}
			echo("</table>");
		}
		
		/**
		 * Escreve a tabela em HTML
		 */
		function writeTableXML($tb) {
			$fields = $this->getFields($tb);
			echo("</tabledef name=\"" . $tb . "\">");
			while($fld = mysqli_fetch_array($fields)) {
				echo("\t<field name=\"" . $fld[0] . "\"></field>");
			}
			echo("</tabledef>");
		}
		
		/*function getApp($ida) {
			$sql = "SELECT * FROM apps WHERE IDApp = $ida";
			
			//echo($sql);
			
			$rs = $this->getData($sql);
			
			if ($row = mysqli_fetch_object($rs)) {
				return $row;
			}
			
			return FALSE;
		}*/
		
		function getDB($iddb) {
			
			$nums = explode(':', $iddb);
			
			if (count($nums) != 2) return false;
			
			return $this->msyql_conns_configs[$nums[0]];
		}
		
		function getInputType($iddb, $tb, $fld) {
			$sql = "SELECT * FROM inputs i WHERE i.idDb = $iddb AND i.table = '$tb' AND i.field = '$fld'";

			$rs = $this->getAssocArray($sql);

			return $rs;
		}
		
		function getDBConn($iddb) {

			$nums = explode(':', $iddb);

			if (count($nums) != 2) return false;

			$dbc = $this->getDB($iddb);

			$db = $dbc['dbs'][$nums[1]];

			$port = isset($dbc['port']) ? $dbc['port'] : NULL;

			$conn = new MPConn($dbc['host'], $dbc['username'], $dbc['password'], $db, $port);
			$conn->host = $dbc['host'];
			$conn->database = $db;

			return $conn;
		}
	}
