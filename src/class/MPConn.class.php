<?php
	//layer para fazer a ligação à bd
	class MPConn extends MySqli {
		public $host;
		public $database;
		public $i; //identity
		public $r; //result
		public $w; //num rows / affected rows
		public $ok; // connection OK
		public $errs; // array de erros

		public $dump_line_end;

		public function getData($sql) {
			global $errs, $msgs, $logs;

			$logs->debug("[MPConn] getData() :: $sql");

			$this->result = $this->query($sql);

			if ($this->errno) {
				$logs->error("ERR [MPConn] :: " . $this->errno . " - " . $this->error);
			}

			return $this->result;
		}

		public function get_databases() {
			$result = $this->getData("SHOW DATABASES;");

			$ignore_tbs = array('information_schema', 'mysql', 'sys', 'performance_schema');

			$lista = array();

			while($row = $result->fetch_row()) {

				if (in_array($row[0], $ignore_tbs)) continue;

				$lista[] = $row[0];
			}

			return $lista;
		}

		/**
		 * Returns the first line as an object
		 */
		function getObject($sql) {
			$rs = $this->getData($sql);

			return $rs ? $rs->fetch_object() : false;
		}

		/**
		 * Returns the first value of the first line
		 */
		function getValue($sql, $def = '') {
			$rs = $this->getData($sql);

			if ($row = $rs->fetch_row()) return $row[0];

			return $def;
		}
		
		/**
		 * Returns the first line as an associative array
		 */
		function getAssocArray($sql) {
			$rs = $this->getData($sql);
			
			if (!$rs) return false;
			
			return $this->fetch_assoc();
		}
		
		function execute($sql) {
			global $logs;
			
			if (!$this->ok) {return false;}
			
			$logs->debug("[MPConn] execute() :: $sql");
			
			$this->query($sql);

			if ($this->errno) {
				$logs->error("ERR [MPConn] :: " . $this->errno . " - " . $this->error);
				return 0;
			}
			
			$this->i = $this->insert_id;
			$this->w = $this->affected_rows;

			if ($this->i) return $this->i;

			return $this->w;
		}
		
		function escape($txt) {
			return $this->real_escape_string($txt);
		}

		function getLastID() {
			return $this->i;
		}
		
		function hasErr() {
			return (bool) $this->errno;
		}

		function showErrList() {
			foreach($this->error_list as $errT) {
				echo("<font color='#FF0000'>");
				echo($errT);
				echo("</font>");
			}
		}

		function getLastErr() {
			return $this->errno . ' - ' . $this->error;
		}

		function getTables($lk = '') {
			$sql = "SELECT TABLE_NAME, LOWER(TABLE_NAME) AS tb2 FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_TYPE = 'BASE TABLE'";

			if ($lk) {
				$sql .= " AND TABLE_NAME LIKE '" . $lk . "_'";
			}

			$sql .= " ORDER BY 2";

			return $this->getData($sql);
		}
		
		function getTablesNames($lk = '') {
			$tables = $this->getTables($lk);

			$list = array();

			if (!$tables) return $list;

			while($t = mysqli_fetch_object($tables)) {
				$list[] = $t->TABLE_NAME;
			}

			return $list;
		}

		function getViews() {
			$sql = "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_TYPE = 'VIEW' ORDER BY TABLE_NAME";

			return $this->getData($sql);
		}
		
		function getRoutines() {
			
			return false;
			
			$sql = "";
			return $this->getData($sql);
		}

		function isTable($tb) {
			$sql = "SELECT COUNT(TABLE_NAME) AS num FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $this->database. "' AND TABLE_NAME = '$tb' AND TABLE_TYPE = 'BASE TABLE'";
			return $this->getValue($sql);
		}

		function getFields($tb) {
			$rs = $this->getData("SHOW FIELDS FROM $tb");

			$list = array();
			while($rowF = $rs->fetch_row()) {
				$list[] = $rowF[0];
			}

			return $list;
		}

		function getFieldsInfo($tb, $sort = 'ORDINAL_POSITION') {
			$sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb'";

			if ($sort) {
				$sql .= " ORDER BY $sort";
			}

			return $this->getData($sql);
		}
		
		function getFieldsInfoKeysArray($tb, $sort = '') {
			$rs = $this->getFieldsInfo($tb, $sort);
			
			$lista = array();
			
			while($row = mysqli_fetch_assoc($rs)) {
				$lista[$row['COLUMN_NAME']] = $row;
			}
			
			return $lista;
		}
		
		function getFieldInfo($tb, $fld) {
			$sql = "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' AND COLUMN_NAME = '$fld'";
			return $this->getAssocArray($sql);
		}
		
		function getFieldsArray($tb) {
			$fds = $this->getFields($tb);

			if (!$fds) return false;

			$list = array();

			foreach($fds as $f) {
				$list[] = $f[0];
			}

			return $list;
		}
		
		function getFieldsCSV($tb, $glue = ', ') {
			$list = $this->getFieldsArray($tb);
			
			$csv = implode($glue, $list);
			
			return $csv;
		}
		
		function getPKFields($tb) {
			$sql = "SELECT COLUMN_NAME, COLUMN_NAME AS Field FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' AND COLUMN_KEY = 'PRI'";

			$rs = $this->getData($sql);

			$list = array();
			while($rowF = $rs->fetch_row()) {
				$list[] = $rowF[0];
			}

			return $list;
		}
		
		function getPKField($tb) {
			$pkf = $this->getPKFields($tb);
			
			$fld = mysqli_fetch_assoc($pkf);
			
			if ($fld) return $fld['Field'];
			
			return '';
		}
		
		function getNonPKFields($tb) {
			//$sql = "SHOW FIELDS FROM $tb WHERE `Key` <> 'PRI'";
			$sql = "SELECT COLUMN_NAME, COLUMN_NAME AS Field FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' AND COLUMN_KEY <> 'PRI'";
			
			$rs = $this->getData($sql);

			$list = array();
			while($rowF = mysqli_fetch_row($rs)) {
				$list[] = $rowF[0];
			}

			return $list;
		}
		
		function getFieldAtPos($tb, $pos = 1) {
			
			if ($pos < 1) $pos = 1;
			
			$sql = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . $this->database . "' AND TABLE_NAME = '$tb' AND ORDINAL_POSITION <= 1 ORDER BY ORDINAL_POSITION DESC LIMIT 1";
			
			return $this->getValue($sql);
		}
		
		function getFirstField($tb) {
			return $this->getFieldAtPos($tb);
		}
		
		function getCreateInfo($tb) {
			$rs = $this->getData("SHOW CREATE TABLE $tb");
			
			$sql = "";
			if ($row = mysqli_fetch_array($rs)) {
				$sql = $row[1];
			}
			
			return $sql;
		}
		
		private function prepareTableCreateInfo($sql) {

			$rexp = '/\sAUTO_INCREMENT=\d+\s/';
			$sql = preg_replace($rexp, " ", $sql, 1);

			//tornar algumas keywords em uppercase

			$keywords = array('AUTO_INCREMENT', 'COLLATE', 'DEFAULT');

			foreach($keywords as $kw) {
				$sql = preg_replace('/' . $kw . '/i', $kw, $sql);
			}

			$sql = preg_replace('/PRIMARY KEY  \(/i', 'PRIMARY KEY (', $sql);

			$sql .= ';';

			return $sql;
		}

		private function prepareViewCreateInfo($sql) {
			$rexp = '/CREATE .* VIEW/i';

			$sql = preg_replace($rexp, "CREATE OR REPLACE VIEW", $sql);

			$sql .= ';';

			return $sql;
		}

		/**
		 * Returns an array with the CREATE TABLE statements
		 */
		function getAllTablesCreateInfo() {
			
			$lista = array();
			
			$tbs = $this->getTables();
			while($t = mysqli_fetch_array($tbs)) {
				
				$sql = $this->getCreateInfo($t[0]);
				
				$sql = $this->prepareTableCreateInfo($sql);
				
				$sql = "DROP TABLE IF EXISTS `" . $t[0] . '`;' . PHP_EOL . $sql;
				
				$lista[] = $sql;
			}
			
			$vws = $this->getViews();
			while($v = mysqli_fetch_array($vws)) {
				
				$sql = $this->getCreateInfo($v[0]);
				
				$sql = $this->prepareViewCreateInfo($sql);
				
				$lista[] = $sql;
			}
			
			return $lista;
		}
		
		function isNumericType($type) {
			if (preg_match('/^int/i', $type)) return true;
			if (preg_match('/^tinyint/i', $type)) return true;
			if (preg_match('/^smallint/i', $type)) return true;
			if (preg_match('/^mediumint/i', $type)) return true;
			if (preg_match('/^bigint/i', $type)) return true;
			if (preg_match('/^decimal/i', $type)) return true;
			if (preg_match('/^numeric/i', $type)) return true;
			if (preg_match('/^float/i', $type)) return true;
			if (preg_match('/^double/i', $type)) return true;

			return false;
		}

		function isBigTextType($type) {
			if (preg_match('/^tinyblob/i', $type)) return true;
			if (preg_match('/^blob/i', $type)) return true;
			if (preg_match('/^mediumblob/i', $type)) return true;
			if (preg_match('/^longblob/i', $type)) return true;
			if (preg_match('/^tinytext/i', $type)) return true;
			if (preg_match('/^text/i', $type)) return true;
			if (preg_match('/^mediumtext/i', $type)) return true;
			if (preg_match('/^longtext/i', $type)) return true;

			return false;
		}
		
		function getFieldType($tb, $fld) {
			global $logs;;
			
			$sql = "SELECT DATA_TYPE FROM `information_schema`.`COLUMNS` WHERE TABLE_SCHEMA = '" . $this->database . "'";
			$sql .= " AND TABLE_NAME = '$tb'";
			$sql .= " AND COLUMN_NAME = '$fld'";
			
			$val = $this->getValue($sql);
			
			$logs->debug("$tb.$fld = $val");
			
			return $val;
		}
		
		function isFieldNumeric($tb, $fld) {
			return $this->isNumericType($this->getFieldType($tb, $fld));
		}
		
		function isFieldBigText($tb, $fld) {
			return $this->isBigTextType($this->getFieldType($tb, $fld));
		}
		
		/**
		 * Builds the dir name, verifies if the dir exists and creates it.
		 */
		function getDumpPathDir($db, $create = false) {
			$query = "SELECT @@secure_file_priv";

			return $this->getValue($query);
		}
		
		function getDumpFullPath($db, $tb) {
			$path_dir = $this->getDumpPathDir($this->d);
			
			$full_path = $path_dir . "$tb.data.dump";
			
			return $full_path;
		}
		
		function exportTableData($tb) {
			$full_path = $this->getDumpFullPath($this->d, $tb);
			
			echo 'full path: ' . $full_path . '<br>';
			//echo $tb;

			if (is_file($full_path)) {
				echo 'deleting ...<br>';
				unlink($full_path);
			}

			$sql = "SELECT * INTO OUTFILE '" . $full_path . "' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\'' LINES TERMINATED BY '" . $this->dump_line_end . "' FROM $tb";
			$r = $this->execute($sql);
			
			if ($r < 0) {
				echo $this->getLastErr() . '<br>';
			}
			
			//var_dump($r);
			
			//echo PHP_EOL;
		}
		
		function exportAllTablesData() {
			$tbs = $this->getTables();
			while($t = mysqli_fetch_array($tbs)) {
				
				//echo $t[0] . PHP_EOL;
				
				$this->exportTableData($t[0]);
			}
		}
		
		function outputSQLCommandsFromDump() {
			
			$path_dir = $this->getDumpPathDir($this->d);
			$full_path = $path_dir . '_full_dump.sql';
			
			return $full_path;
			
			if (is_file($full_path)) unlink($full_path);
			
			$output = fopen($full_path, 'a');
			fwrite($output, 'use ' . $this->database . ';' . PHP_EOL . PHP_EOL);
			fwrite($output, '-- SET NAMES latin1;' . PHP_EOL . PHP_EOL);
			
			$tbs = $this->getTables();
			while($row = mysqli_fetch_array($tbs)) {
				
				$tb = $row[0];
				
				//echo $tb . PHP_EOL;
				
				$flds_csv = $this->getFieldsCSV($tb);
				
				//echo $flds_csv . PHP_EOL;
				
				$full_path_dump = $this->getDumpFullPath($this->d, $tb);
				
				//echo $full_path_dump . PHP_EOL;
				
				$file_data = file_get_contents($full_path_dump);
				
				//echo $file_data . PHP_EOL . PHP_EOL;
				
				$lines = explode($this->dump_line_end, $file_data);
				
				//$lines = file($full_path_dump, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
				
				foreach($lines as $l) {
					
					if (strlen($l) < 1) continue;
					
					$sql = "INSERT INTO $tb ($flds_csv) VALUES ($l);" . PHP_EOL;
					
					fwrite($output, $sql);
				}
				
				fwrite($output, PHP_EOL);
				
				//echo PHP_EOL . PHP_EOL;
			}
			
			fclose($output);
			
			return $full_path;
		}
	}
