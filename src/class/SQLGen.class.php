<?php
	class SQLGen {
		public $c;
		
		public function __construct($cn) {
			$this->c = $cn;
		}
		
		public function getSELECT1($tb) {
			$sql = "SELECT * FROM $tb";
			
			return $sql;
		}
		
		public function getSELECT2($tb) {
			$sql = 'SELECT ${flds} FROM $tb';
			
			$flds = $this->c->getFields($tb);
			
			$strF = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $strF .= ", ";
				
				$strF .= $f[0];
				$b = true;
			}
			
			$sql = preg_replace('{\${flds}}', $strF, $sql);
			
			return $sql;
		}
		
		public function getINSERT($tb) {
			$sql = 'INSERT INTO $tb (${flds}) VALUES (${vls})';
			
			$flds = $this->c->getNonPKFields($tb);
			
			$strF = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $strF .= ", ";
				
				if ($f[5] != "auto_increment") {
					$strF .= $f[0];
					$b = true;
				}
			}
			
			$flds = $this->c->getNonPKFields($tb);
			$strV = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $strV .= ", ";
				
				if ($f[5] != "auto_increment") {
					//echo("'$" .$f[0] . "'");
					$strV .= "'$" .$f[0] . "'";
					$b = true;
				}
			}
			
			$sql = preg_replace('{\${flds}}', $strF, $sql);
			$sql = preg_replace('{\${vls}}', $strV, $sql);
			
			return $sql;
		}
		
		public function getINSERT_lang($tb) {
			
			$list = $this->c->getFieldsArray($tb);
			$csv = implode(', ', $list);
			
			$sql = "INSERT INTO $tb ($csv) VALUES (";
			
			$sql .= str_repeat('?, ', count($list) - 1);
			
			$sql .= "?)";
			
			foreach($list as $f) {
				//$sql = str_replace('?', $f, $sql, 1);
				
				if ($this->c->isFieldNumeric($tb, $f)) {
					$sql = preg_replace('/\?/', "\" . \$" . $f . "[\$l->lang] . \"", $sql, 1);
				} else {
					$sql = preg_replace('/\?/', "'\" . \$" . $f . "[\$l->lang] . \"'", $sql, 1);
				}
			}
			
			return $sql;
		}
		
		public function getPHP_INSERT_lang($tb) {
			
			$pk = $this->c->getPKField($tb);
			
			$php = '$sql = "";' . PHP_EOL;
			$php .= '$sql = "INSERT INTO ' . $tb . ' SET";' . PHP_EOL;
			
			$list = $this->c->getFieldsArray($tb);
			$first = true;
			foreach($list as $f) {
				
				if ($f == $pk) continue;
				
				$isNum = $this->c->isFieldNumeric($tb, $f);
				
				$php .= "\$sql = \"";
				
				if (!$first) {
					$php .= ',';
				} else {
					$first = false;
				}
				
				$php .= " $f = ";
				
				if ($isNum) {
					$php .= "\$$f\"";
				} else {
					$php .= "'\$$f'\"";
				}
				
				$php .= ';' . PHP_EOL;
			}
			
			return $php;
		}
		
		public function getUPDATE($tb) {
			$sql = 'UPDATE ' . $tb . ' SET ${flds} WHERE ${pks}';
			
			$flds = $this->c->getNonPkFields($tb);
			
			$strF = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $strF .= ", ";
				
				$strF .= $f[0] . " = '$" . $f[0] . "'";
				$b = true;
			}
			
			$flds = $this->c->getPkFields($tb);
			
			$pkF = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $pkF .= ", ";
				
				$pkF .= $f[0] . " = '$" . $f[0] . "'";
				$b = true;
			}
			
			$sql = preg_replace('{\${flds}}', $strF, $sql);
			$sql = preg_replace('{\${pks}}', $pkF, $sql);
			
			return $sql;
		}
		
		public function getDELETE($tb) {
			$sql = 'DELETE FROM ' . $tb . ' WHERE ${pks}';
			
			$flds = $this->c->getPkFields($tb);
			
			$pkF = "";
			$b = false;
			while($f = mysqli_fetch_array($flds)) {
				if ($b) $pkF .= ", ";
				
				$pkF .= $f[0] . " = '$" . $f[0] . "'";
				$b = true;
			}
			
			$sql = preg_replace('{\${pks}}', $pkF, $sql);
			
			return $sql;
		}
		
		// DELETE ALL TABLES
		public function getDELETEALLTABLES() {
			$sql = "";
			$tbs = $this->c->getTables();
			$b = false;
			while($t = mysqli_fetch_array($tbs)) {
				$sql .= "DELETE FROM " . $t[0] . ";" . PHP_EOL;
			}
			
			return $sql;
		}
	}
