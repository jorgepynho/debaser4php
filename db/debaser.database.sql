
CREATE SCHEMA `debaser` DEFAULT CHARACTER SET utf8 ;

USE debaser;

CREATE TABLE `apps` (
  `idApp` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDDB` int(10) unsigned NOT NULL DEFAULT '0',
  `Name` varchar(31) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  PRIMARY KEY (`idApp`)
) ENGINE=MyISAM;

CREATE TABLE `dbs` (
  `IDDB` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Server` varchar(63) NOT NULL DEFAULT '',
  `dbname` varchar(31) NOT NULL DEFAULT '',
  `user` varchar(31) NOT NULL DEFAULT '',
  `pwd` varchar(31) NOT NULL DEFAULT '',
  PRIMARY KEY (`IDDB`)
) ENGINE=MyISAM;

INSERT INTO dbs SET Server = 'localhost', dbname = 'debaser', user = 'root', pwd = 'root';

CREATE TABLE `inputs` (
  `idInput` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idDb` int(10) unsigned NOT NULL DEFAULT '0',
  `table` varchar(31) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `field` varchar(31) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `type` varchar(31) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux1` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux2` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux3` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux4` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux5` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux6` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux7` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux8` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `aux9` varchar(63) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`idInput`)
) ENGINE=MyISAM;

CREATE TABLE `menus` (
  `idMenu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idApp` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(15) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  `sqlCmd` varchar(255) COLLATE latin1_general_cs NOT NULL DEFAULT '',
  PRIMARY KEY (`idMenu`),
  UNIQUE KEY `UniqueTitle` (`idApp`,`title`)
) ENGINE=MyISAM;

